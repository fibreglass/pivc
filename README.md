# Introduction

Welcome to PIVC, a program for performing planar Particle Image Velocimetry (PIV) computations written in C/C++. This is an open source program that uses the Fast Fourier Transform (FFT) to compute two-dimensional velocity vectors from image pairs. This program uses OpenMP to leverage the abilities of multi-core CPUs. A brief overview of the operations PIVC performs is presented in the [analysis algorithm](#analysis-algorithm) section of this document. For more background and details on PIVC please see paper.md or [the draft paper in pdf format](https://github.com/openjournals/joss-papers/blob/joss.03736/joss.03736/10.21105.joss.03736.pdf). Users unfamiliar with the basics of PIV are encouraged to read a brief [introduction to PIV](https://gitlab.com/fibreglass/pivc/-/blob/master/PIVINTRO.md) and learn about some past applications of PIVC in the [demonstrations](https://gitlab.com/fibreglass/pivc/-/blob/master/DEMO.md). 

# Precompiled PIVC

Using the precompiled binary is the fastest way to use PIVC. The precompiled software is compatible with 64-bit operating systems using x86-64 CPUs with limited CPU specific optimizations to achieve broad compatibility. Precompiled binaries are provided in both multi-threaded and single-threaded forms. In order to obtain maximum performance users should compile the source code. Users looking to quickly test PIVC should read the [Demonstration documentation](https://gitlab.com/fibreglass/pivc/-/blob/master/DEMO.md) and follow the steps outlined in the [Demo Usage](https://gitlab.com/fibreglass/pivc/-/blob/master/DEMO.md#usage) section.

## GNU/Linux
To use the precompiled software GNU/Linux users need download the "PIVC-GNULinux.zip" file from the most recent [release](https://gitlab.com/fibreglass/pivc/-/releases) and extract it to a directory of their choice. Next, users need to ensure all [dependencies](https://gitlab.com/fibreglass/pivc/-/blob/master/COMPILING.md) are installed prior to running PIVC. 

## Windows
To use the precompiled software Windows users need download the "PIVC-Windows.zip" file from the most recent [release](https://gitlab.com/fibreglass/pivc/-/releases) and extract it to a directory of their choice. All of the .dll files provded in the release are required to be in the same folder as the executable. These .dll files are the dependencies of PIVC and users do not need to install or configure any additional software to use precompiled PIVC on Windows. 

# PIVC Demonstration

A demonstrative script that executes the multi-threaded PIVC is included in the release that will process the sample images in the "demo" folder and generate plain text output files. The demonstration covers the application of PIVC in two experimental flows and two synthetic analysis cases. For details on the demonstrations, see the [Demo details](https://gitlab.com/fibreglass/pivc/-/blob/master/DEMO.md).

# Compiling PIVC

For details on compiling PIVC, see the [Compiling](https://gitlab.com/fibreglass/pivc/-/blob/master/COMPILING.md) documentation. 

# PIVC Usage
In the directory where PIVC has been compiled or extracted, the program can be run from terminal as follows. All input parameters are required however the order of specifying parameters does not matter. 

```sh
./PIVC -g [grid spacing] -c [compression] -i [inputDirectory] -o [outputFilename] -s [coordinateSystem]
```
Example
```sh
./PIVC -g 16 -c plain -i "/path/to/images" -o "/path/to/data/filename" -s imageproc
```
* Windows users should omit "./" prior to use.

## Input parameters
**Grid spacing**: This is one half of the interrogation window size and one quarter of the search window size. Use only one of these 5 quantities: 8, 12, 16 (default), 24, 32. PIVC performs the PIV mathematical analysis with 50% overlap between interrogation windows and search windows. This overlap is fixed as it offers the best accuracy of computed vectors in the implemented algorithm, hence the interrogation window size cannot be adjusted without changing the grid spacing. Decreasing grid spacing causes a roughly linear increase computation time (varies with PC hardware) and decreasing the grid spacing from 32 to 8, produces a 16 times increase in the number of vectors computed.

Grid spacing lower than 16 is not reccomended without careful experimental control. While it is possible to set the grid spacing to 8, caution must be used in experimental setup as often the inherent noise in the images captured by a camera can become significant relative to the signal originating from relfective seed particles. In addition, very small values of grid spacing increase the liklihood of producing errorneous results due to a large magnitude of seed particle displacement in the fluid flow.

In the computed vector field, adjacent vectors will be separated by a distance (in pixels) equal to grid spacing. For a new application, it is recommended to start with a grid spacing of 32 and then decrease the grid spacing as needed. If grid spacing is set too low, there will appear to be vectors indicating fluid flow is moving in an manner inconsistent with expected outcome. This tends to occur if the computed particle displacement (pixel shift) in one direction is larger than about 50% to 60% of the selected grid spacing value. 

**Compression**: This is a flag to set output file compression (gzip format) or uncompressed data. Valid parameters are "plain" (default) and "gz". Case sensitive. Output data compression uses the zlib library to achieve data compression and maintain broad compatibility with other languages (Java, MATLAB, Python, etc). 

**Input Directory**: This is the full path to a folder containing your input images. This program will attempt to process all files in the folder in alphabetical order. Keep your file names consistent. E.g. Image_0001.tif, Image_0002.tif, etc. All images must be 8 bit or up to 16 bit tiff, uncompressed or compressed, with no alpha channel. The default is the current directory.

**Output File Name**: This is the full path to a folder where you want your data to be written including the desired filename. E.g. "/home/Frank/franksdata" to produce output files "/home/Frank/franksdata001.piv". Data is written in a comma separated values style with line breaks separating rows and commas separating adjacent values in each row. Default is the current directory with the filename "pivdata".

**Coordinate System**: This is a flag that cooresponds to the coordinate system of the output data. Valid parameters are "cartesian" and "imageproc". Case sensitive. The "imageproc" option is the default and will output data in conventional image processing coordinates (i.e. origin on the top left with down and to the right as positive). The "cartesian" option will output data in Cartesian coordinates (i.e. origin on the bottom left with up and to the right as positive).

## Analysis algorithm
The following is a brief overview of the steps PIVC performs to determine a 2D vector field from a pair of images. Full details are presented in previous works cited in paper.md or [the draft paper](https://github.com/openjournals/joss-papers/blob/joss.03736/joss.03736/10.21105.joss.03736.pdf). For more information on the code structure in this implementation see the [Developers Guide](https://gitlab.com/fibreglass/pivc/-/blob/master/DEVGUIDE.md).

* Image Input: In a directory containing images whose names are Image_0001.tif, Image_0002.tif, Image_0003.tif, Image_0004.tif, and etc., images are loaded one pair at a time. That is Image_0001.tif and Image_0002.tif are loaded and used to compute vector field 1, then Image_0003.tif and Image_0004.tif are loaded and used to compute vector field 2. At this time it is not possible for PIVC to process all consecutive images to produce vector fields, that is, processing Image_0001.tif and Image_0002.tif to produce vector field 1 and then processing Image_0002.tif and Image_0003.tif to produce vector field 2. 

* Preprocessing: Each image is preprocessed using these two basic steps. 1. Brightness and constrast adjustment. 2. Convolution using a 3-by-3 Gaussian kernel. Users can perform their own image pre-processing such as cropping, background subtraction, and etc. prior to using PIVC with 3rd party tools.

* Vector computation: Vector computation is performed in five steps. 
    1. Each image in the pair is split into many interrogation windows (for the first image in the pair) and search windows (for the second image in the pair). Each interrogation window and search window has the respective average gray level subtracted.
    2. The FFT-based cross-correlation is performed on corresponding interrogation windows and search windows. 
    3. The cross-correlation maxima are determined with sub-pixel accuracy for each interrogation window and corresponding search window. 
    4. The 2D vector corresponding to a given interrogation window and search window is computed from the location of cross-correlation maxima. 
    5. Each computed 2D vector is verified using a central mean (i.e. mean of adjacent vectors) test.

* Data Output: The computed vector field is written to either a plain-text or GZip compressed text data file. 

## How to use the data output files
Data is written in a 10 column plain or gzip compressed text file

* Column 1: Subpixel location of vector (x)
* Column 2: Subpixel location of vector (y)
* Column 3: Horizontal pixel shift (x-direction)
* Column 4: Vertical pixel shift (y-direction)
* Column 5: Deviation from central mean
* Column 6: Signal-to-noise ratio
* Column 7: The correlation peak height
* Column 8: A vector quality flag
* Column 9: Horizontal coordinate of vector (x) on regular grid
* Column 10: Vertical coordinate of vector (y) on regular grid

In order to load data plain text output files into MATLAB or Octave use the following command

```sh
data = load('datafile.piv');
```

This loads the contents of datafile.piv into the variable "data".

In order to load compressed output files into MATLAB or Octave use the following commands. 

```sh
fileName = 'datafile.piv.gz';
fileNameShort = strrep(fileName,'.gz','');
gunzip(fileName);
data = load(fileNameShort);
delete(fileNameShort); 
```

This extracts the contents of datafile.piv.gz and stores in the current directory under the name datafile.piv. Next the compressed file is loaded into the variable "data". Finally the uncompressed file is deleted.

Note that the compressed output files can be opened the way any archive can be opened (e.g. using 7zip, etc)

Plotting of vector fields is achieved by using columns 3, 4, 9, and 10 with graphing software. In MATLAB or Octave this can achieved using 

```sh
quiver(data(:,9),data(:,10),data(:,3),data(:,4),0,'k'); 
axis ij;
```

This generates a 2D vector field of black vectors (input parameter: 'k') with no scaling (input parameter: 0). The "axis ij" command is used to orient the vector field in a Cartesian coordinate orientation. PIVC outputs coordinates according to the digital image processing convention (i.e. top left is the origin) which is in contrast to the origin in Cartesian coordinates (i.e. bottom left). 

# For Developers

Users and developers interested in the program structure of PIVC should read the [Developers Guide](https://gitlab.com/fibreglass/pivc/-/blob/master/DEVGUIDE.md) and [Contributing](https://gitlab.com/fibreglass/pivc/-/blob/master/CONTRIBUTING.md) documents. This guide is to help developers learn the structure of PIVC while the algorithm details have been [previously reported](https://github.com/openjournals/joss-papers/blob/joss.03736/joss.03736/10.21105.joss.03736.pdf) by the authors. 
