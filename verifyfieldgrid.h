#ifndef VERIFYFIELDGRID_H
#define VERIFYFIELDGRID_H

struct VectorFinalParams { //parameters for vector finalization
    int meanAreaSize; // CMAREAS
    double peakThreshold; //peakThreshold
    double offsetx;
    double offsety;
    int gridSpacing;
    double resetVal; //RESETVAL
    int xGrids;
    int yGrids;
};

void VerifyFieldGrid(double *field, double *peaks, struct VectorFinalParams params);
void iFlipExtendVField(double *extfield, double *field, int* fields, int dif);
void iVerifyFieldGrid(double *d, double *extfield, double *field, int *fields, int &mareas, double &threshold, double &offsetx, double &offsety, int &griddis, double &resetval);
void CentralMean(double* d, double* velmatrix, int &number, int &mid, int &start, double* xval, double* yval);
#endif // VERIFYFIELDGRID_H
