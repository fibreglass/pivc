# Compiling PIVC 

Compiling PIVC is only needed for users seeking to customize the source code or obtain maximum performance. Note that performance improvements from compiling the source code with modified compiler flags may vary. The source code can be downloaded from the most recent [release](https://gitlab.com/fibreglass/pivc/-/releases) or cloned using Git. Users wishing to make modifications to PIVC source code should review the [tester](https://gitlab.com/fibreglass/pivc/-/blob/master/README-TEST.md), [Developers Guide](https://gitlab.com/fibreglass/pivc/-/blob/master/DEVGUIDE.md), and [Contributing](https://gitlab.com/fibreglass/pivc/-/blob/master/CONTRIBUTING.md) documents.

# GNU/Linux

## Dependencies
- Any recent C/C++ compiler that supports the C++20 standard (GCC 10 or newer preferred) and OpenMP 4.0 or newer (supported in GCC 6).
- LibTiff 4.2.0 or newer
- FFTW 3.3.x or newer
- ZLib 1.2.10 or newer 
- Any recent Argp (included in libc)

## Build instructions

- Download the source code from the newest release or clone using Git. Users should clone/download from the master branch.

- Install the dependencies from your distribution's package manager

- Optional: modify the makefile using a text editor to suit your usage case. For example to produce a single threaded version when using GCC, remove -fopenmp and -lgomp from the makefile. The specific flags you use may vary with your choice of compiler. For maximum performance use compiler flags specific to your CPU. The broad -march=native and -mtune=native flags are the default in the makefile to serve as a starting point.

- Navigate to the source code directory then run 
```sh
make
```
# Windows

## Windows Subsystem for Linux (WSL)
- Users have the option of using Windows Subsystem for Linux (WSL) to compile PIVC. Users who prefer not to use WSL or are unfamilar with WSL are reccomended to use the steps for Windows 7 and above. 

- Follow the Linux installation instructions, that is installing dependencies and running "make". The specific commands will vary with your choice of Linux distribution within WSL. 

## Windows 7 and above when not using WSL
- Do not download or clone the PIVC repository
- Download and install MSYS2. Follow the Getting Started guide here: https://www.msys2.org/
- Complete steps 1 through 7 then continue below

- After completing the getting started guide, install Git from here https://packages.msys2.org/queue
- To do this you need to run "MSYS2 MSYS" from Start menu  
- to install Git run the following in the MSYS2 MSYS window
```sh
pacman -S git
```
- After Git is installed you can now close the MSYS2 MSYS window.

- Optional: modify the makefile using a text editor to suit your usage case. For example to produce a single threaded version, remove -fopenmp and -lgomp from the makefile. GCC has many compiler options that are well documented (<a href="https://gcc.gnu.org/onlinedocs/gcc/Optimize-Options.html">Optimization</a> and <a href="https://gcc.gnu.org/onlinedocs/gcc-11.2.0/gcc/x86-Options.html">x86 CPU options</a>). The default PIVC makefile uses a broadly applicable selection of compiler flags, specifically -march=native and -mtune=native flags to serve as a starting point. The script contained in the makefile assumes you have installed MSYS in the default configuration. If you have a non-default configuration for MSYS, then you need to modify the second line in the makefile to reflect your system.

- To download, configure, and compile PIVC first run "MSYS MinGW 64-bit" from Start menu. You will need to be in the directory where you want to have the PIVC source code stored. For example, you can run the following in the MINGW64 window to navigate to the location where you intend to store PIVC.

```sh
cd "/c/Users/YourName/Desktop/pivc"
```

- The download, configuration, and compiling of PIVC is performed through MSYS MinGW. To download and perform the default configuration of PIVC, run the following the MINGW64 window.

```sh
git clone --branch windows https://gitlab.com/fibreglass/pivc.git
make configure-initial
```

- The initial configuration downloads the latest version of the dependencies of PIVC for Windows. The dependencies of PIVC for Windows are as follows 
* LibTiff 4.2.0 or newer
* FFTW 3.3.x or newer
* ZLib 1.2.10 or newer 
* TCLAP 1.2.x or newer

- In addition you will notice that the PIVC directory has been populated by several .dll files. These files were generated when you installed MSYS as well as the dependencies of PIVC. These files can be found in "C:/msys64/mingw64/bin" (assuming default configuration) and are named as follows. Note that version numbers may change as each of the listed dependencies is updated in MSYS. 
* libdeflate.dll
* libfftw3-3.dll
* libgcc_s_seh-1.dll
* libgomp-1.dll
* libjbig-0.dll
* libjpeg-0.dll
* liblzma-5.dll
* libstdc++-6.dll
* libtiff-5.dll
* libwebp-7.dll
* libwinpthread-1.dll
* libzstd.dll
* zlib1.dll

- These .dll files are required dependencies of PIVC for Windows and must be in the same folder as the PIVC executable.

- After configuration, PIVC can then be compiled by running 

```sh
make 
```

- You will produce a standalone executable called PIVC.exe located in the folder where compilation was performed. You can now close the MINGW64 window.

### Updating PIVC on Windows
- To simplify the process of tracking updates on PIVC for Windows, users have the option of running the following from MINGW64

```sh
make update
```

- This command will update MSys, download the newest version of the PIVC source code for Windows, download and update all dependencies, update all .dll files, and recompile PIVC. 
