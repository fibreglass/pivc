#include "filemanip.h"
#include "preprocess.h"

//Wrapper file that detects bit-depth in a given tiff image then loads to appropriate image and returns the image to the caller
//Input: filename and the image dimensions
//Returns: 1D array containing the image data (gray scale value at each pixel) in double precision format
//Advisory: input tif file must be readable in scanline/strip but not tile format. Tif file must be uncompressed, LZW, Packbits, or deflate compressed. If compression causes issues then stay with uncompressed.
double* myTiffRead(string fileName, uint32 &sizeX, uint32 &sizeY)
{
    TIFF* tif = TIFFOpen(fileName.data(), "r");
    double* result=0;
    if(tif)
    {
        uint16 bitdepth;
        TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitdepth);
        if(bitdepth == 8)
            result = myTiffRead8Bit(tif, sizeX, sizeY); //8 bit images only
        else
            result = myTiffRead16Bit(tif, sizeX, sizeY); //10 to 16 bit images stored in 16-bit tif container
    }
    else
    {
        cout<<"Image file not found or other image IO error. Now terminating."<<endl;
        exit(-1);
    }
    TIFFClose(tif);
    return result;
}

//Loads 16-bit tiff file (no alpha channel, grayscale only) into a 2D array. 10, 12, 14 bit files are treated as 16 bit.
//Input: the tif file which is already open for reading and the image dimensions
//Returns: 1D array containing the image data (gray scale value at each pixel) in double precision format
double* myTiffRead16Bit(TIFF* tif, uint32 &sizeX, uint32 &sizeY)
{
    double* result = 0;
    tdata_t buffer;
    uint32 row;
    uint16* data;
    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &sizeX);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &sizeY);
    buffer = _TIFFmalloc(TIFFScanlineSize(tif));
    result = new double[sizeX*sizeY];
    for(row = 0; row < sizeY; row++)
    {
        TIFFReadScanline(tif, buffer, row);
        data = (uint16*)buffer;
        for(uint32 j = 0; j < sizeX; j++)
            result[sub2Dind(row,j,sizeX)] = (double) data[j];
    }
    _TIFFfree(buffer);
    return result;
}

//Loads 8-bit tiff file (no alpha channel, grayscale only) into a 2D array
//Input: the tif file which is already open for reading and the image dimensions
//Returns: 1D array containing the image data (gray scale value at each pixel) in double precision format
double* myTiffRead8Bit(TIFF* tif, uint32 &sizeX, uint32 &sizeY)
{
    double* result = 0;
    tdata_t buffer;
    uint32 row;
    uint8* data;
    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &sizeX);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &sizeY);
    buffer = _TIFFmalloc(TIFFScanlineSize(tif));
    result = new double[sizeX*sizeY];
    for(row = 0; row < sizeY; row++)
    {
        TIFFReadScanline(tif, buffer, row);
        data = (uint8*)buffer;
        for(uint32 j = 0; j < sizeX; j++)
            result[sub2Dind(row,j,sizeX)] = (double) data[j];
    }
    _TIFFfree(buffer);
    return result;
}

//Gets a list of files in the specified directory and tells you how how many files are in the directory
//List is returned in sorted order. Uses the C++20 filesystem functions.
//Input: a reference to the  list of files to be populated, the directory to be searched, a reference to the number of files
//Returns: the sorted list of filenames (first input parameters), number of files in the directory (third input parameter)
void getFiles(vector<string> &fileList, string myDir, int &numFiles)
{
    numFiles = 0;
    for(auto& p: fs::directory_iterator(myDir))
    {
        fileList.push_back(p.path().string());
        numFiles++;
    }
    sort(fileList.begin(),fileList.end());
}

//Original Tiff read implementation retained for legacy and testing purposes
double* myTiffReadOld(string fileName, uint32 &sizeX, uint32 &sizeY)
{
    TIFF* tif = TIFFOpen(fileName.data(), "r");
    double* result = 0;
    if (tif) //if the file exists
    {
        size_t npixels;
        uint32* raster;

        TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &sizeX);
        TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &sizeY);
        npixels = sizeX * sizeY;
        raster = (uint32*) _TIFFmalloc(npixels * sizeof (uint32));
        if (raster != NULL)
        {
            if (TIFFReadRGBAImage(tif, sizeX, sizeY, raster, 0))
            {
                result = new double[npixels];
                for(uint32 i = 0; i < sizeY; i++)
                    for(uint32 j = 0; j < sizeX; j++)
                        result[sub2Dind(i,j,sizeX)] = (double) TIFFGetR(raster[(sizeY-1-i)*sizeX + j]);
            }
            _TIFFfree(raster);
        }
        TIFFClose(tif);
    }
    else //if no file is found or there is some other error like file permissions then stop execution.
    {
        cout<<"Image file not found or other image IO error. Now terminating."<<endl;
        exit(-1);
    }
    return result;
}
