#include "preprocess.h"
#include "dowholefield.h"

using namespace std;
//Computing the raw displacements occurs in the dowholefield function
//Input: image A -- input image used for interrogation window
//Input: image B -- input image used for search window
//Input: params -- processing parameters such as the image sizes, window sizes, and etc.
//Return: peaks -- 4D array containing the cross-correlation peaks and pixel shifts of each vector
void DoWholeField(double* peaks,double* imageA,double* imageB,struct VectorCompParams params)
{
    double res = params.gridSpacing; //typecast from int to double here
    double startingPos[] = {params.offsetx,params.offsety}; //starting location in interrogation image

    #pragma omp parallel
    {
        double pos[] = {params.offsetx,params.offsety}; //current ordered pair of coordinates within the interrogation image (not window)
        double* spip = new double[params.searchWindowSize*params.searchWindowSize]; //current search window
        double* ipip = new double[params.interWindowSize*params.interWindowSize]; //current interrogation window
        double* v = new double[9]; //current computed vectors (contains the top 3 vectors with 2D pixel shifts and cross-correlation peaks)
        fill_n(spip,params.searchWindowSize*params.searchWindowSize,0);
        fill_n(ipip,params.interWindowSize*params.interWindowSize,0);
        fill_n(v,9,0);
        #pragma omp for
        for(int i = 0; i < params.xGrids; i++) // go through picture in x direction in parallel
        {
            pos[0] = startingPos[0] + res*i;
            pos[1] = startingPos[1];

            for(int j = 0; j < params.yGrids; j++) // go through picture in y direction
            {
                iCopyAoiInIP(imageA, pos, ipip, params.interWindowSize, params.extendedSizeX); //copy image section into interrogation window
                iCopyAoiInIP(imageB, pos, spip, params.searchWindowSize, params.extendedSizeX); //copy image section into search window
                DetDisp(v, ipip, spip, params.searchWindowSize, params.interWindowSize); //compute the top 3 vectors for these windows
                for(int k = 0; k < 3; k++)	//copy vectors into peaks
                    for(int l = 0; l < 3; l++)
                        peaks[sub4Dind(j,i,k,l,params.yGrids,params.xGrids,3)] = v[sub2Dind(k,l,3)];

                pos[1] += res;
            }
        }
        delete[] v;
        delete[] spip;
        delete[] ipip;
    }
    fftw_cleanup(); //This cleans up from the FFT (See FFTW's wisdom and documentation on this function)
}
//Intermediately function to compute vectors. Simply passes along data to the right places and calls other functions
void DetDisp(double* v, double* ipip, double* spip, int &nspip, int &nipip)
{
    int msize = nspip - nipip + 1; //compute the size of the cross-correlation matrix
    double *matrix = new double[msize*msize]; //make the cross-correlation matrix
    fill_n(matrix,msize*msize,0);
    CrossCorFft(matrix, msize, ipip, nipip, spip, nspip); //compute the FFT cross-correlation
    iMax3(v, matrix, msize); //select the 3 best vectors from this cross-correlation matrix
    delete[] matrix;
}

void iMax3(double *d, double *cmatrix, int &msize)
{
    int sdum = msize-1;
    double minheight = -100000.2;		// set very small numbers as initial max peak height. The .2 at the end matters
    double oldmin = -100000.2;
    int halfsize = msize/2; //Truncation due to typecast is on purpose. Casting in the next line to handle warnings (narrowing)
    double ddum[3][3] = {{(double) halfsize, (double) halfsize,-100000.2},{(double) halfsize, (double) halfsize,-100000.1}, {(double) halfsize, (double) halfsize,-100000.0}};
    if(cmatrix[sub2Dind(0,0,msize)] == cmatrix[sub2Dind(1,1,msize)]) cmatrix[sub2Dind(0,0,msize)] -= 0.01;	// preprocessing of edges (edges are ignored)
    if(cmatrix[sub2Dind(0,sdum,msize)] == cmatrix[sub2Dind(1,sdum-1,msize)]) cmatrix[sub2Dind(0,sdum,msize)] -= 0.01;

    for(int i=1; i < sdum; i++) //preprocessing edges
    {
        if(cmatrix[sub2Dind(0,i,msize)] == cmatrix[sub2Dind(1,i-1,msize)]) cmatrix[sub2Dind(0,i,msize)] -= 0.01;
        if(cmatrix[sub2Dind(0,i,msize)] == cmatrix[sub2Dind(1,i,msize)]) cmatrix[sub2Dind(0,i,msize)] -= 0.01;
        if(cmatrix[sub2Dind(0,i,msize)] == cmatrix[sub2Dind(1,i+1,msize)]) cmatrix[sub2Dind(0,i,msize)] -= 0.01;
        if(cmatrix[sub2Dind(i,0,msize)] == cmatrix[sub2Dind(i-1,1,msize)]) cmatrix[sub2Dind(i,0,msize)] -= 0.01;
        if(cmatrix[sub2Dind(i,0,msize)] == cmatrix[sub2Dind(i,1,msize)]) cmatrix[sub2Dind(i,0,msize)] -= 0.01;
        if(cmatrix[sub2Dind(i,0,msize)] == cmatrix[sub2Dind(i+1,1,msize)]) cmatrix[sub2Dind(i,0,msize)] -= 0.01;
    }

    for(int i=1; i < sdum; i++)
    {	// edges are not allowed
        for(int j=1; j < sdum; j++)
        {
            if(cmatrix[sub2Dind(i,j,msize)] > minheight)
            {
                if((cmatrix[sub2Dind(i,j,msize)] > cmatrix[sub2Dind(i-1,j,msize)]) && (cmatrix[sub2Dind(i,j,msize)] > cmatrix[sub2Dind(i-1,j+1,msize)]) &&
                    (cmatrix[sub2Dind(i,j,msize)] >= cmatrix[sub2Dind(i,j+1,msize)]) && (cmatrix[sub2Dind(i,j,msize)] >= cmatrix[sub2Dind(i+1,j+1,msize)]) &&
                    (cmatrix[sub2Dind(i,j,msize)] >= cmatrix[sub2Dind(i+1,j,msize)]) && (cmatrix[sub2Dind(i,j,msize)] >= cmatrix[sub2Dind(i+1,j-1,msize)]) &&
                    (cmatrix[sub2Dind(i,j,msize)] > cmatrix[sub2Dind(i,j-1,msize)]) && (cmatrix[sub2Dind(i,j,msize)] > cmatrix[sub2Dind(i-1,j-1,msize)]))
                { //if the current location in cmatrix is bigger than the adjacent 8-connected points
                    minheight = cmatrix[sub2Dind(i,j,msize)]; //set this point to be the current max and store it in ddum along with the previous 2 biggest values
                    for(int k=0; k < 3; k++)
                    {
                        if(fabs(ddum[k][2] - oldmin) < 0.0001)
                        {
                            ddum[k][0] = j;
                            ddum[k][1] = i;
                            ddum[k][2] = cmatrix[sub2Dind(i,j,msize)];
                        }
                        if(ddum[k][2] < minheight) minheight = ddum[k][2];
                    }
                    oldmin = minheight;
                }
            }
        }
    }

    SortPeaks(d, ddum);		// sort the 3 peaks from largest to smallest

    if(d[sub2Dind(0,2,3)] > -10000.0) //The highest peak will be determined with sub-pixel accuracy
        SubPixDet(d[sub2Dind(0,0,3)], d[sub2Dind(0,1,3)], d[sub2Dind(0,0,3)], d[sub2Dind(0,1,3)], cmatrix[sub2Dind((int)d[sub2Dind(0,1,3)], (int)d[sub2Dind(0,0,3)],msize)], cmatrix[sub2Dind((int)d[sub2Dind(0,1,3)],(int)d[sub2Dind(0,0,3)]-1,msize)], cmatrix[sub2Dind((int)d[sub2Dind(0,1,3)]-1,(int)d[sub2Dind(0,0,3)],msize)], cmatrix[sub2Dind((int)d[sub2Dind(0,1,3)],(int)d[sub2Dind(0,0,3)]+1,msize)], cmatrix[sub2Dind((int)d[sub2Dind(0,1,3)]+1,(int)d[sub2Dind(0,0,3)],msize)]);
    else{		// if no peak was found the peak is assumed at the center
        d[sub2Dind(0,0,3)] = msize/2;
        d[sub2Dind(0,1,3)] = msize/2;
        d[sub2Dind(0,2,3)] = -100000.0; // set intensity to very low value indicating that no suitable peak was found
    }

    // move to centre coordinates that is move the origin (where pixel shift is measured from) from the top left corner of cmatrix, to the center of cmatrix
    int offset = msize/2;
    for(int k=0; k < 3; k++)
    {
        d[sub2Dind(k,0,3)] = d[sub2Dind(k,0,3)]-offset;
        d[sub2Dind(k,1,3)] = d[sub2Dind(k,1,3)]-offset;
    }
}

// function to calculate the parameters of a Gaussian correlation peak
//Input parameters here are the coordinates of the peak (x,y). Peak value (gv00) and the values from cmatrix in the 4 points adjacent to cmatrix in a 4-connected region
void SubPixDet(double &d0, double &d1, int x, int y, double gv00, double gv0m1,double gvm10, double gv0p1, double gvp10)
{
    double lngv00, lngv0m1, lngvm10, lngv0p1, lngvp10;
    double mini;

    double data[4] = {gv0m1, gv0p1, gvm10, gvp10};
    mini = *min_element(data,data+4);
    // too small and negative (which are possible !) values have to be avoided.
    lngv00 = log(gv00 + 20.0 - mini);
    lngv0m1 = log(gv0m1 + 20.0 - mini);
    lngvm10 = log(gvm10 + 20.0 - mini);
    lngv0p1 = log(gv0p1 + 20.0 - mini);
    lngvp10 = log(gvp10 + 20.0 - mini);
    if(fabs(lngv0m1+lngv0p1-2.0*lngv00) < 0.0001)
        d0 = (double)x;
    else
        d0 = (double)x + (lngv0m1-lngv0p1)/(2.0*(lngv0m1+lngv0p1-2.0*lngv00)); // x0  -- subpixel coordinate in x
    if(fabs(lngvm10+lngvp10-2.0*lngv00) < 0.0001)
        d1 = (double)y;
    else
        d1 = (double)y + (lngvm10-lngvp10)/(2.0*(lngvm10+lngvp10-2.0*lngv00)); // y0 -- subpixel coordinate in y
}

//Performs sorting of the 3 rows in peaks (input parameter) based on the value stored in peaks[][2]
void SortPeaks(double *d, double peaks[3][3])
{
    if(peaks[0][2] < peaks[1][2])
    {
        if(peaks[0][2] < peaks[2][2])
        {
            if(peaks[1][2] > peaks[2][2])
                for(int k=0; k < 3; k++)
                {
                    d[sub2Dind(2,k,3)] = peaks[0][k];
                    d[sub2Dind(0,k,3)] = peaks[1][k];
                    d[sub2Dind(1,k,3)] = peaks[2][k];
                }
            else
                for(int k=0; k < 3; k++)
                {
                    d[sub2Dind(2,k,3)] = peaks[0][k];
                    d[sub2Dind(0,k,3)] = peaks[2][k];
                    d[sub2Dind(1,k,3)] = peaks[1][k];
                }
        }
        else
            for(int k=0; k < 3; k++)
            {
                d[sub2Dind(1,k,3)] = peaks[0][k];
                d[sub2Dind(0,k,3)] = peaks[1][k];
                d[sub2Dind(2,k,3)] = peaks[2][k];
            }
    }
    else
    {
        if(peaks[0][2] > peaks[2][2])
        {
            if(peaks[1][2] > peaks[2][2])
                for(int k=0; k < 3; k++)
                {
                    d[sub2Dind(0,k,3)] = peaks[0][k];
                    d[sub2Dind(1,k,3)] = peaks[1][k];
                    d[sub2Dind(2,k,3)] = peaks[2][k];
                }
            else
                for(int k=0; k < 3; k++)
                {
                    d[sub2Dind(0,k,3)] = peaks[0][k];
                    d[sub2Dind(1,k,3)] = peaks[2][k];
                    d[sub2Dind(2,k,3)] = peaks[1][k];
                }
        }
        else
            for(int k=0; k < 3; k++)
            {
                d[sub2Dind(1,k,3)] = peaks[0][k];
                d[sub2Dind(2,k,3)] = peaks[1][k];
                d[sub2Dind(0,k,3)] = peaks[2][k];
            }
    }
}
//Performs the FFT based cross-correlation using the interrogation windows (ipip) and search window (spip)
//Returns the raw FFT cross-correlation matrix (named matrix)
void CrossCorFft(double* matrix, int &msize, double* ipip, int &isize, double* spip, int &ssize)
{
    int fftsize = NextPof2(ssize); //We use 2D DFT sizes that the first power of 2 larger than the search window size
    int offset = 0;
    double *sext = new double[fftsize*fftsize];
    double *iext = new double[fftsize*fftsize];
    fill_n(iext,fftsize*fftsize,0);
    fill_n(sext,fftsize*fftsize,0);

    double nfactor = imageMean(spip,ssize*ssize); //get the average gray level of the search window
    iSubCopyInAoi(spip, nfactor, sext, offset, ssize, fftsize); //copy search window into 2D DFT sized matrix (centered)

    nfactor = imageMean(ipip,isize*isize); //get the average gray level of the interrogation window
    offset = (ssize-isize)/2;
    iSubCopyInAoi(ipip, nfactor, iext, offset, isize, fftsize); //copy interrogation window into 2D DFT sized matrix (centered)

    fftw_complex *resultA,*resultB;
    fftw_plan myPlan;
    resultA = fftw_alloc_complex(fftsize*((fftsize/2)+1));
    #pragma omp critical
    myPlan = fftw_plan_dft_r2c_2d(fftsize,fftsize,iext,resultA,FFTW_ESTIMATE); //perform 2D DFT on interrogation window
    fftw_execute(myPlan);
    #pragma omp critical
    fftw_destroy_plan(myPlan);

    resultB = fftw_alloc_complex(fftsize*((fftsize/2)+1));
    #pragma omp critical
    myPlan = fftw_plan_dft_r2c_2d(fftsize,fftsize,sext,resultB,FFTW_ESTIMATE); //perform 2D DFT on search window
    fftw_execute(myPlan);
    #pragma omp critical
    fftw_destroy_plan(myPlan);

    fftw_complex *resConj;
    resConj = addConj(resultA, resultB,fftsize,fftsize); //perform the cross-correlation on the 2D DFT output
    double *realRes = fftw_alloc_real(fftsize*fftsize);

    #pragma omp critical
    myPlan = fftw_plan_dft_c2r_2d(fftsize,fftsize,resConj,realRes,FFTW_ESTIMATE); //inverse 2D DFT on the cross-correlated output
    fftw_execute(myPlan);
    double* complete = reshape2DFFT(realRes, fftsize, fftsize); //reorganize the contents of the inverse DFT result to match the column major format here

    fftw_free(resultA);
    fftw_free(resultB);
    fftw_free(resConj);
    fftw_free(realRes);
    #pragma omp critical
    fftw_destroy_plan(myPlan);

    iExtrMatr(matrix, complete, msize, fftsize); //copy the inverse 2D DFT result into the output variable and perform a reorganization
    delete[] sext;
    delete[] iext;
    delete[] complete;
}

//Calculate the mean value of the given image, or array in general.
double imageMean(double* image, int length)
{
    double sum = 0;
    for(int i = 0; i < length; i++)
        sum += image[i];

    return sum / (double) length;
}

//Performs the cross-correlation on the 2D DFT outputs. Note that the cross-correlation in Fourier domain simply means multiplying by the complex conjugate
fftw_complex* addConj(fftw_complex *vectorA, fftw_complex *vectorB, int &sizeX, int &sizeY)
{
    int length = sizeX * ((sizeY/2)+1);
    int scaleFactor = sizeX*sizeY*sizeY*sizeX;
    fftw_complex *result = fftw_alloc_complex(length);

    for(int i = 0; i < sizeX; i++) //due to symmetry of DFT on a real image
    {
        vectorA[i*((sizeX/2)+1) + sizeY/2][0] = 0; //real
        vectorA[i*((sizeX/2)+1) + sizeY/2][1] = 0; //imaginary
        vectorB[i*((sizeX/2)+1) + sizeY/2][0] = 0; //real
        vectorB[i*((sizeX/2)+1) + sizeY/2][1] = 0; //imaginary
    }

    for(int i = 0; i < length; i++) //multiply A by complex conjugate of B
    {
        result[i][0] = (vectorA[i][0] * vectorB[i][0] + vectorA[i][1]*vectorB[i][1])/scaleFactor; //real part
        result[i][1] = (vectorA[i][1] * vectorB[i][0] + vectorA[i][0]*vectorB[i][1]*-1)/scaleFactor; //imaginary part but conjugated
    }
    return result;
}

//This function simply reorganizes the contents of the input array
//FFTW uses row major order like MATLAB. This problem uses column major order. This function simply performs that reorganization and returns the result
double* reshape2DFFT(double* inArray, int &sizeY, int &sizeX)
{
    double* image = new double[sizeX*sizeY];
    for(int i = 0; i < sizeY; i++)
    {
        for(int j = 0; j < sizeX; j++)
        {
            if(i == 0)
            {
                if(j == 0)
                    image[sub2Dind(i,j,sizeY)] = inArray[i*sizeX + j];
                else
                    image[sub2Dind(i,sizeX-j,sizeY)] = inArray[i*sizeX + j];
            }
            else
            {
                if(j == 0)
                    image[sub2Dind(sizeX-i,j,sizeY)] = inArray[i*sizeX + j];
                else
                    image[sub2Dind(sizeX-i,sizeX-j,sizeY)] = inArray[i*sizeX + j];
            }
        }
    }
    return image;
}

//This function copies the contents of result into matrix. The size of each array are also input parameters.
//This function also organizes the result variable into a form where the center of matrix contains the zero frequency output
//MATLAB's fftshift also performs this reorganization
void iExtrMatr(double *matrix, double *result, int &msize, int &fftsize)
{
    int dum1, dum2, dum3;
    dum2 = msize/2;
    dum1 = msize-dum2;
    dum3 = fftsize-dum2;

    for(int i=0; i < dum1; i++)
        for(int j=0; j < dum1; j++)
            matrix[sub2Dind(i+dum2,j+dum2,msize)] = result[sub2Dind(i,j,fftsize)]; //put the zero frequency content in the center

    for(int i=0; i < dum2; i++) //fill in the rest of matrix while performing the fftshift
    {
        for(int j=0; j < dum2; j++)
            matrix[sub2Dind(i,j,msize)] = result[sub2Dind(i+dum3,j+dum3,fftsize)];
        for(int j=0; j < dum1; j++)
        {
            matrix[sub2Dind(i,j+dum2,msize)] = result[sub2Dind(i+dum3,j,fftsize)];
            matrix[sub2Dind(j+dum2,i,msize)] = result[sub2Dind(j,i+dum3,fftsize)];
        }
    }
}

//Returns the next power of 2 greater than the input quantity
int NextPof2(int &number)
{
    if (number > 64)
        return 128;
    else if (number > 32)
        return 64;
    else if (number > 16)
        return 32;
    else if (number > 8)
        return 16;
    else if (number > 4)
        return 8;
    else return 4;
}

//Copies the contents of ipip (image) into iext (ready for DFT). Subtracts the average value of the image (nfactor) before copying.
void iSubCopyInAoi(double *ipip, double &nfactor, double *iext, int &offset, int &isize, int &extsize)
{
    for(int i = 0; i < isize; i++)
        for(int j = 0; j < isize; j++)
            iext[sub2Dind(i+offset,j+offset,extsize)] = ipip[sub2Dind(i,j,isize)] - nfactor;
}

//Copies a section of an image (im) into an interrogation or search window (pip).
//Uses only discrete windows
void iCopyAoiInIP(double *im, double *pos, double *pip, int pips, int sws)
{
    int offsetx1, offsety1;
    double hpips, gm;
    double lagreyl;// Local Average Grey Level;

    // Discrete Window only
    offsetx1 = floor(pos[0] - pips/2.0 + 0.5);
    offsety1 = floor(pos[1] - pips/2.0 + 0.5);
    lagreyl = 0.0;
    for(int i = 0; i < pips; i++)
    {
        for(int j = 0; j < pips; j++)
        {
            pip[sub2Dind(i,j,pips)] = im[sub2Dind(i+offsety1,j+offsetx1,sws)];
            lagreyl += pip[sub2Dind(i,j,pips)];
        }
    }

    lagreyl /= (pips*pips); //calculate the local average gray level for this window
    hpips = pips/2.0;

    for(int i = 0; i < pips; i++) //Applies a Gaussian Mask to the window based on the window's average gray level
    {
        for(int j = 0; j < pips; j++)
        {
            pip[sub2Dind(i,j,pips)] = pip[sub2Dind(i,j,pips)] - lagreyl;
            gm = exp(-2.0*((i+1.0-hpips)*(i+1.0-hpips)/(hpips*hpips)+(j+1.0-hpips)*(j+1.0-hpips)/(hpips*hpips)));
            pip[sub2Dind(i,j,pips)] *= gm;
        }
    }
}
