# Developers Guide

This is the developers guide to PIVC. This guide is intended to outline the structure of PIVC's programming to help faclitate understanding and accelerate future development. Developers seeking more detail than presented in this document are encouraged to read the comments in the respective code files.

# Overview

A high level overview of PIVC is presented in Figure 1. 

<h2 id="F1"></h2>
<img src="/docsrc/DevGuideFig1.png" alt="Figure 1" title="Figure 1" width="600"/> <figcaption>Figure 1: Overview of PIVC program structure.</figcaption>

The analysis procedure starts with the `workspace.cpp` file which acts as the "main" program that defines the sequence of operations in PIVC. The `workspace.cpp` program takes user input by interfacing with `inputParser.c` and accepts image files provided by `filemanip.cpp`. Next, `workspace.cpp` passes the raw images to `preprocess.cpp` where the images are filtered and prepared for analysis. The filtered images are passed to `dowholefield.cpp`. In this file, the FFT-based cross-correlation is performed to determine the raw pixel shift. This raw pixel shift data is passed to `verifyfieldgrid.cpp` where displacement field vectors are determined. The verified displacement field is returned to `workspace.cpp` where it is passed to `PIVWriter.cpp` for writing to disk.

The multi-threaded version of PIVC utilizes OpenMP to achieve parallelism with `preprocess.cpp` and `dowholefield.cpp`. 

# The "Main" Program (workspace.cpp) 

Upon execution of PIVC, the `main` function in `workspace.cpp` is called and this immediately calls the `inputHandle` function from `inputParser.c`. After receiving user input, the file writer object as defined by `PIVWriter.cpp` is created based on user input. Next, image files are prepared for loading via the `getFiles` function in `filemanip.cpp`. The `main` function begins looping through all input image files. At each iteration, a pair of images are loaded using `myTiffRead` from `filemanip.cpp`. These images are then filtered (`preprocess.cpp`), have raw pixel shifts computed (`dowholefield.cpp`), and have the displacement fields determined (`verifyfieldgrid.cpp`). The resulting data is finally written to the specified output file with the `writeData` function in `PIVWriter.cpp`. Once all image files have been processed, the `main` function reports total elapsed time and terminates.

## User Interface (inputParser.c)

On the GNU/Linux distribution, `inputParser.c` interacts with the argp library. On Windows, this file is named `inputParser.cpp` and the tclap library is used. Regardless of operating system, the `inputParser` program handles all user input from command line and sets default values as follows 

* Grid Spacing: 16 pixels
* Coordinate system: image processing
* Output file format: plain text
* Input directory: current directory
* Output directory: current directory with "pivdata" filename

A structure containing the parsed input data is returned to `workspace.cpp`. 

## Image File I/O (filemanip.cpp)

This file contains multiple functions for handling image file input. The `getFiles` function uses the filesystem library in C++20 to query the user-supplied directory and return a sorted list of filenames and the quantity of files to be loaded. The `myTiffRead` function uses the TIFF library to load the image into a double-precision array. Low (8-bit) and high (up to 16-bit) bit depth images are loaded using separate functions, `myTiffRead8Bit` and `myTiffRead16Bit` respectively.

## Data File I/O (PIVWriter.cpp)

PIVWriter is written in object oriented style to enable future growth to other file formats. Subclasses of PIVWriter must implement the `writeData` function. The PIVWriter class provides a generic constructor and `setStreamLength` function for basic file name configuration. Presently, PIVC supports plain and compressed (using zlib) data output using either image processing or cartesian coordinate systems (based on user input). The `workspace.cpp` program creates a PIVWriter object suited for either plain or compressed output based on user input. After data analysis for a given image pair is complete, `workspace.cpp` passes output data and a file number as parameters to the `writeData` function. This function opens and writes the given data to a file in the user-selected coordinate system.

# Preprocessing (preprocess.cpp)

The image preprocessing section of PIVC accepts one raw image at a time and several parameters stored in the `preprocessparams` structure defined within `workspace.cpp`. These preprocessing parameters are as follows.

* Image dimensions: `imageSizeX`, `imageSizeY`, `extendedSizeX`, `extendedSizeY`
* Minimum and maximum image gray levels: `minGrayLevel` and `maxGrayLevel`

In `workspace.cpp`, an output array (`imageA` and `imageB`) is created and used to store the filtered image. The `preProcessImage` function in `preprocess.cpp` takes the raw image (`imageOneRaw` and `imageTwoRaw`), output array, and `preprocessparams` as input parameters. It then performs a brightness and contrast adjustment on the input image using the `minGrayLevel` and `maxGrayLevel` parameters (see `iStretchIm` function). Next, 2D convolution using a separable Gaussian mask is performed on the input image in `myConv2Same`. Finally, `iReplacetoExtIm` copies the input image into the output array. It is important to note that the output array is an image that is larger than the input image. Specifically the output array (image) contains the filtered and contrast enhanced input image in the center with a black border on all sides whose width is one half the size of the search window. 

In the multi-threaded version of PIVC, the preprocessing of each image is performed in parallel.

## On Array Format in PIVC

In its original form, PIVC made extensive use of multi-dimensional arrays. An image for example, can be simply understood as a two-dimensional array. During development, it was found that multi-dimensional arrays are computationally expensive. Hence multi-dimensional arrays have been replaced with 1D arrays that are indexed using the `sub2Dind`, `sub3Dind`, and `sub4Dind` functions for 2D, 3D, and 4D arrays respectively.

# Vector Displacement (dowholefield.cpp)

The vector displacement section in `dowholefield.cpp` performs the FFT-based cross-correlation in order to determine the raw displacement field. The function `DoWholeField` is called by `workspace.cpp` where it is supplied with the filtered input images, computation parameters (`vectorcompparams`), and a 4D output array (`peaks`). The computation parameters are as follows:

* Interrogation window size: `interWindowSize`
* Search window size: `searchWindowSize`
* Grid spacing: `gridSpacing`
* Number of interrogation windows in each direction: `xGrids` and `yGrids`
* Image dimensions: `extendedSizeX`
* Image size offsets: `offsetx` and `offsety`

In the `DoWholeField` function the two filtered images, hereafter referred to as image A and image B, will have the cross-correlation applied. This process begins with defining the starting location within image A (known as `pos` which contains `offsetx` and `offsety`) where image A will be divided into interrogation windows. Arrays are created to store the interrogation window, `ipip`, and search window, `spip`, from image A and image B respectively. An output array to store the cross-correlation for the current interrogation window, `v`, is also created. 

The next steps are performed for all interrogation windows in image A. In multi-threaded PIVC, interrogation windows are analyzed in parallel. First, the function `iCopyAoiInIP` copies the current interrogation window from image A into `ipip`, the same process occurs for the search window from image B into `spip`. Second, the `DetDisp` function accepts `v`, `ipip`, `spip`, and their respective sizes and performs the FFT-based cross-correlation using the FFTW library, see `CrossCorFft` function. This function first subtracts the average gray level of `ipip` and `spip` from their respective arrays (see `iSubCopyInAoi` function and `imageMean` function). Then, the FFT (2D DFT) is performed `ipip` and `spip` followed by the cross-correlation, in the Fourier domain this is the same as multiplication by the complex conjugate (see `addConj` function). The inverse FFT is then performed on the cross-correlation (output of `addConj` function) and the output is reorganized using the `reshape2DFFT` then copied to the `matrix` variable (a 2D matrix of cross-correlation values) using the `iExtrMatr` function.

The computed cross-correlation 2D matrix (`matrix`) is then analyzed using the `iMax3` function. This function determines the indices (row and column) of the three largest values of the cross-correlation matrix where sorting is performed by the `SortPeaks` function. The largest value in the cross-correlation matrix, has it's index computed to sub-pixel accuracy using a 2D Gaussian distribution, see `SubPixDet` function. The indices (i.e. pixel shifts) of the three largest cross-correlation values and the respective correlation values themselves are stored in `v`. Finally, the contents of `v` (2D array) are copied into `peaks` (4D array) for the current interrogation window. The current interrogation window is identified by 2D coordinates (indices) within `peaks`. 

After all interrogation windows and their corresponding search windows have had the cross-correlation performed, the results stored in `peaks` are then used by `verifyfieldgrid.cpp`.

# Vector Verification (verifyfieldgrid.cpp)

The final section of PIVC is to finalize the pixel shifts computed in `dowholefield.cpp` into a displacement field. It is important to note that, `dowholefield.cpp` outputs three potential displacement vectors (corresponding to the three largest values of cross-correlation) for each interrogation window. The purpose of `verifyfieldgrid.cpp` is to select the best displacement vector (pixel shift) for each interrogation window. This part of PIVC is single threaded,  The input parameters to the `VerifyFieldGrid` function from `workspace.cpp` are a 3D array, `velocityField`, for storing the output, the 4D array from `dowholefield.cpp`, `peaks`, and a structure of input parameters, `vectorfinalparams`, which contains the following:

* Central mean area size: `meanAreaSize`
* Peak threshold: `peakThreshold`
* Image size offsets: `offsetx` and `offsety`
* Grid spacing: `gridSpacing`
* Number of interrogation windows in each direction: `xGrids` and `yGrids`
* Flag to identify detected vectors: `resetVal`

Within `verifyfieldgrid.cpp`, the first step is to copy the contents of `peaks` into `extfield` which is performed by the `iFlipExtendVField` function. Next, the `iVerifyFieldGrid` function performs several operations for each interrogation window. First, the three potential displacement vectors at a given interrogation window are compared to the potential displacement vectors of the adjacent displacement vectors by performing a central mean test (see `CentralMean`). The vector that best satisfies both of the following conditions is selected as the best vector for the given interrogation window:

* The vector minimizes the difference between itself and the central mean (i.e. mean pixel shift of adjacent vectors) 
* The difference between itself and the central mean is greater than `peakThreshold` 

For the selected vector, the following quantities are copied into the output variable, `velocityField`. 

* The selected vector's pixel shift (Column 3 and 4 in the output file)
* Ratio of selected vector's cross-correlation value to the next largest cross-correlation value (Column 6 in the output file)
* Sub-pixel coordinates (Column 1 and 2 in the output file)
* Interrogation window coordinates (Column 9 and 10 in the output file)
* Difference between the selected vector and the central mean (Column 5 in the output file)
* Selected vector's cross-correlation value or zero is no satisfactory vector is found (Column 7 in output file)
* A flag (0 = unsatisfactory or 1 = satisfactory) to identify selected vector quality (Column 8 in output file)

Once all interrogation windows have been processed, the completed displacement field is returned to `workspace.cpp`. This data is then passed to `PIVWriter.cpp` for file writing. Finally, PIVC cleans up the data from the current image pair analysis and moves on to the next image pair or terminates if no further images are available for analysis. 