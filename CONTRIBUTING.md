# For Contributors

## Bug Reports and Feature Requests
Report bugs to the issue tracker. Requests for new features and asking questions about the design and operation of PIVC are welcomed. Be as detailed and specific as possible when reporting bugs and requesting new features.

## Code Contributions
Contributions to this program are welcome. For major contributions please contact the developers beforehand and read the [Developers Guide](https://gitlab.com/fibreglass/pivc/-/blob/master/DEVGUIDE.md), we may already be working on the same thing you want to do. External contributions are managed through the <a href="https://guides.github.com/activities/forking/">fork and pull</a> workflow. Be aware that any contributions made to PIVC will be shared under GPLv3 license.

### Steps to Contribute
* First open an Issue to discuss the proposed contribution.
* Create your own local fork of PIVC and implement the changes.
* Create a request to merge the changes into the main project. 
* Changes will be reviewed and discussed with you before being merged into the main repository.
