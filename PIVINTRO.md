# Introduction to PIV

Particle Image Velocimetry (PIV) is an optical technique that remotely measures fluid velocity. The most common implementation of PIV is planar PIV where two fluid velocity components are simultaneously measured in a two-dimensional plane. The elements of a modern digital planar PIV system include a suitable seed particle, PC, laser, synchronization unit, laser optics, and digital camera. A laser light beam exits the laser unit and passes through a focusing lens followed by a cylindrical lens. This forms the focused laser beam into a thin ($`<`$ 2 mm thick) light sheet. The digital camera is placed facing the light sheet and the camera lens is focused on the sheet as shown in  <a href="#F1">Figure 1</a>. In an experiment seed particles previously added to the fluid, such as plastic beads in water or oil droplets in air, pass through the laser light sheet where they reflect light. This reflected light is captured by the camera. 

<h2 id="F1"></h2>
<img src="/docsrc/Fig1.png" alt="Figure 1" title="Figure 1" width="600"/> <figcaption>Figure 1: Sketch of a typical PIV setup.</figcaption>

The laser light is emitted in very short pulses, typically a few nanoseconds, and each pulse is timed using a synchronization unit so that each laser pulse occurs during the exposure time of each frame captured by the camera. The synchronization unit faclitates a user-configured time between consecutive laser light pulses ($`\Delta t`$) and the camera's frame rate. A timeline of one potential image acqusition system is shown in <a href="#F2">Figure 2</a>. The flexibility of the PIV implementation allows for a wide range of fluid phenomena time scales to be captured and analyzed. 

<h2 id="F2"></h2>
<img src="/docsrc/Fig2.png" alt="Figure 2" title="Figure 2" width="600"/> <figcaption>Figure 2: Sketch of typical PIV setup light source and image capture timeline.</figcaption>

PIV image analysis is performed on consecutive pairs of images. In a given pair, the first image is split into many "interrogation windows" and the second image is split into several larger "search windows" where each interrogation window uniquely corresponds to one search window. A sample image pair with one interrogation window and search window is presented in <a href="#F3">Figure 3</a>. Frame one was captured first and a short time delay ($`\Delta t`$) later, frame two was recorded. The different sizes of the search window and interrogation window allows for rigid translation of the interrogation window within the search window (see <a href="#F3">Figure 3</a>). The 2D cross-correlation, in PIVC this is the FFT based cross-correlation, between the interrogation window and search window is computed for all rigid translations ($`\Delta i, \Delta j`$) of the interrogation window within the search window.

<h2 id="F3"></h2>
<img src="/docsrc/Fig3.png" alt="Figure 3" title="Figure 3" width="600"/> <figcaption>Figure 3: Illustration of PIV interrogation and search windows.</figcaption>

The rigid translation of the interrogation window that corresponds to the largest cross-correlation magnitude, as shown in <a href="#F4">Figure 4</a>, is taken as the estimated displacement of the interrogation window. Specifically, this displacement defines the distance the seed particle captured in the interrogation window has translated during the time between each laser pulse ($`\Delta t`$). By dividing the computed displacement by the time between laser pulses, the local planar velocity is estimated. This process is repeated for all interrogation and search windows in the image pair producing a two-dimensional velocity field. By capturing and analyzing a large collection of images the characteristics of the fluid velocity field can be described in both space and time.

<h2 id="F4"></h2>
<img src="/docsrc/Fig4.png" alt="Figure 4" title="Figure 4" width="600"/> <figcaption>Figure 4: Visualization of computed cross-correlation showing a strong peak at the estimated particle displacement. Adapted from Raffel et. al. [1]</figcaption>

[1] M. Raffel, C. E. Willert, S. Wereley, and J. Kompenhans. Particle Image Velocimetry: A Practical Guide. Springer, 2007.