#include "workspace.h"
#include "preprocess.h"
#include "dowholefield.h"
#include "verifyfieldgrid.h"
#include "PIVWriter.h"
#include "filemanip.h"

//The workspace.cpp file is the main operator for the other files. It has the purpose of tying all the steps together and calculating run time.

int main(int argc, char **argv)
{
    struct arguments args;
    args = inputHandler(argc, argv); //Handle user input

    int gridSpacing = args.grid; //grid spacing
    PIVWriter *generalWriter; //generic file writer

    if(args.argComp == plain)
        generalWriter = new PlainPIVWriter(args.argOrder,args.output_path); //create plain file writer
    else
        generalWriter = new GZPIVWriter(args.argOrder,args.output_path);    //create compressed file writer

    struct PreProcessParams preprocessparams;
    struct VectorCompParams vectorcompparams;
    struct VectorFinalParams vectorfinalparams;

    int numFiles = 0;
    vector<string> fileList;
    uint32 imageSizeY;
    uint32 imageSizeX;
    getFiles(fileList, args.input_path, numFiles);
    generalWriter->setStreamLength(numFiles); //length of output stream format

    cout<<"Now processing "<<numFiles<<" files."<<endl;

    double *peaks, *velocityField, *imageA, *imageB, *imageOneRaw, *imageTwoRaw;

    auto start = chrono::steady_clock::now(); //start timing
    for(int fileCounter = 0; fileCounter < numFiles; fileCounter+=2) //For all files in the directory
    {

       imageOneRaw = myTiffRead(fileList[fileCounter],imageSizeX,imageSizeY); //Load a pair of image files
       imageTwoRaw = myTiffRead(fileList[fileCounter+1],imageSizeX,imageSizeY);

       if(fileCounter == 0) //Initialize parameters on the first iteration
        {
            vectorcompparams.interWindowSize = 2*gridSpacing; //IPIPS
            vectorcompparams.searchWindowSize = 2*vectorcompparams.interWindowSize; //SPIPS
            vectorcompparams.gridSpacing = gridSpacing; //gridspacing
            vectorcompparams.extendedSizeX = imageSizeX + vectorcompparams.searchWindowSize; //extendedSizeX
            vectorcompparams.xGrids = imageSizeX / gridSpacing; //xgrids
            vectorcompparams.yGrids = imageSizeY / gridSpacing; //ygrids

            preprocessparams.extendedSizeY = imageSizeY + vectorcompparams.searchWindowSize; //extendedSizeY

            int xGap = imageSizeX - vectorcompparams.xGrids*gridSpacing;
            int yGap = imageSizeY - vectorcompparams.yGrids*gridSpacing;

            vectorcompparams.offsetx = -0.5 + ((vectorcompparams.extendedSizeX - imageSizeX) + xGap + vectorcompparams.interWindowSize)/2.0; //xoffset
            vectorcompparams.offsety = -0.5 + ((preprocessparams.extendedSizeY - imageSizeY) + yGap + vectorcompparams.interWindowSize)/2.0; //yoffset

            preprocessparams.imageSizeY = imageSizeY;
            preprocessparams.imageSizeX = imageSizeX;
            preprocessparams.extendedSizeX = vectorcompparams.extendedSizeX;
            preprocessparams.minGrayLevel = 0;
            preprocessparams.maxGrayLevel = 200;

            vectorfinalparams.meanAreaSize = 3; // CMAREAS
            vectorfinalparams.peakThreshold = 2; //peakThreshold
            vectorfinalparams.offsetx = ((xGap + vectorcompparams.interWindowSize)/2.0) - 0.5;
            vectorfinalparams.offsety = ((yGap + vectorcompparams.interWindowSize)/2.0) - 0.5;
            vectorfinalparams.gridSpacing = gridSpacing;
            vectorfinalparams.resetVal = 1; //resetval
            vectorfinalparams.xGrids = vectorcompparams.xGrids;
            vectorfinalparams.yGrids = vectorcompparams.yGrids;
        }

        imageA = new double[preprocessparams.extendedSizeY*preprocessparams.extendedSizeX]; //initialize preprocessed images
        fill_n(imageA,preprocessparams.extendedSizeY*preprocessparams.extendedSizeX,0);
        imageB = new double[preprocessparams.extendedSizeY*preprocessparams.extendedSizeX];
        fill_n(imageB,preprocessparams.extendedSizeY*preprocessparams.extendedSizeX,0);

        #pragma omp parallel sections num_threads(2)
        {
              #pragma omp section
              preProcessImage(imageA, imageOneRaw, preprocessparams); //preprocess image one

              #pragma omp section
              preProcessImage(imageB, imageTwoRaw, preprocessparams); //preprocess image two
        }

        peaks = new double[vectorcompparams.yGrids*vectorcompparams.xGrids*3*3]; //initialize the 4D structure with vector field data
        fill_n(peaks,vectorcompparams.yGrids*vectorcompparams.xGrids*3*3,0);
        DoWholeField(peaks,imageA,imageB,vectorcompparams); //compute the possible vectors at each location using FFT cross-correlation

        velocityField = new double[vectorcompparams.yGrids*vectorcompparams.xGrids*10]; //initialize the 3D structure with finalized vectors
        fill_n(velocityField,vectorcompparams.yGrids*vectorcompparams.xGrids*10,0);
        VerifyFieldGrid(velocityField,peaks,vectorfinalparams); //finalize the vector field

        generalWriter->writeData(fileCounter,velocityField,vectorcompparams.yGrids,vectorcompparams.xGrids,10); //write vector field data to disk

        cout<<"Vector field "<<1+(fileCounter/2)<<" of "<<(numFiles/2)<<" processed."<<endl;

        delete[] imageA; //clean up this iteration
        delete[] imageB;
        delete[] imageOneRaw;
        delete[] imageTwoRaw;
        delete[] peaks;
        delete[] velocityField;
    }

    auto end = chrono::steady_clock::now(); //stop timing
    auto diff = end - start; //get elapsed time
    cout<<"Elapsed time "<<chrono::duration <double> (diff).count() <<" seconds"<<endl; //print elapsed time
    return 0;
}
