#include "PIVWriter.h"
#include "preprocess.h"

PIVWriter::PIVWriter(DataOrder order, string outDir)
{
    myOrder = order;
    outPrefix = outDir;
}
void PIVWriter::setStreamLength(int numFiles)
{
    streamLength = 1 + log10(numFiles);
}

int PlainPIVWriter::writeData(int fileCounter, double* data, int sizeX, int sizeY, int sizeZ)
{
    ostringstream fileNameStream;
    fileNameStream<<outPrefix<<setfill('0')<<setw(streamLength)<<1+(fileCounter/2)<<".piv"; //make the complete output file name

    ofstream myFile;
    myFile.open(fileNameStream.str());
    if(!myFile)
    {
        cout<<"Output file is not valid or output IO error."<<endl;
        return -1;
    }
    int y = 0;
    int z = 0;
    for(int x = 0; x < sizeX; x++)
    {
        while(y < sizeY)
        {
            if(myOrder == cartesian)
            {
                if(z == 3)
                    myFile << setprecision(8) << -1*data[sub3Dind((sizeX-1)-x,y,z,sizeX,sizeY)];
                else if(z == 8 || z == 9)
                    myFile << setprecision(8) << data[sub3Dind(x,y,z,sizeX,sizeY)];
                else
                    myFile << setprecision(8) << data[sub3Dind((sizeX-1)-x,y,z,sizeX,sizeY)];
                //vertical direction flips hence vertical vector sign must change (index of 3) while horizontal direction is unchanged
                //the subtraction on the y-coordinate performs the flip from imageproc to cartesian coords
                //the coordinates at index 8 and 9 do not change, only the vectors saved at that location
            }
            else
                myFile << setprecision(8) << data[sub3Dind(x,y,z,sizeX,sizeY)]; //Using 8 digits total to be written to the output file.

            if(z == sizeZ-1)
            {
                myFile << endl;
                y++;
                z = 0;
            }
            else
            {
                myFile << ",";
                z++;
            }
        }
        y = 0;
        z = 0;
    }
    myFile.close();
    return 0;
}

int GZPIVWriter::writeData(int fileCounter , double* data, int sizeX, int sizeY, int sizeZ)
{
    ostringstream fileNameStream;
    fileNameStream<<outPrefix<<setfill('0')<<setw(streamLength)<<1+(fileCounter/2)<<".piv.gz"; //make the complete output file name

    gzFile myFileGZ2 = gzopen(fileNameStream.str().data(),"wb4"); //Level 4 compression offers good balance of speed and file size.
    if(!myFileGZ2)
    {
        cout<<"Output file is not valid or output IO error."<<endl;
        exit(-1);
    }
    stringstream currentLine;
    int y = 0;
    int z = 0;
    for(int x = 0; x < sizeX; x++)
    {
        while(y < sizeY) //assemble a row of data to be written to the file
        {
            if(myOrder == cartesian)
            {
                if(z == 3)
                    currentLine << setprecision(8) << -1*data[sub3Dind((sizeX-1)-x,y,z,sizeX,sizeY)];
                else if(z == 8 || z == 9)
                    currentLine << setprecision(8) << data[sub3Dind(x,y,z,sizeX,sizeY)];
                else
                    currentLine << setprecision(8) << data[sub3Dind((sizeX-1)-x,y,z,sizeX,sizeY)];
                //vertical direction flips hence vertical vector sign must change (index of 3) while horizontal direction is unchanged
                //the subtraction on the y-coordinate performs the flip from imageproc to cartesian coords
                //the coordinates at index 8 and 9 do not change, only the vectors saved at that location
            }
            else
                currentLine << setprecision(8) << data[sub3Dind(x,y,z,sizeX,sizeY)]; //Using 8 digits total to be written to the output file.

            if(z == sizeZ-1)
            {
                currentLine << endl;
                y++;
                z = 0;
            }
            else
            {
                currentLine << ",";
                z++;
            }
        }
        gzwrite(myFileGZ2,currentLine.str().data(),currentLine.tellp()); //then write the row to the file
        currentLine = stringstream(); //and start a new row of data to be written
        y = 0;
        z = 0;
    }
    gzclose(myFileGZ2);
    return 0;
}
