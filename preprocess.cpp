#include "preprocess.h"
#include <iostream>
//Preprocessing of images. Performs brightness/contrast adjustment and convolution on supplied image
//Input: rawImage -- raw input image
//Input: params -- structure of parameters
//Return: processedImage -- output image
void preProcessImage(double* processedImage, double* rawImage, struct PreProcessParams params)
{
    //Find min and max values of rawImage
    double theMax = *max_element(rawImage,rawImage+(params.imageSizeY*params.imageSizeX));
    double theMin = *min_element(rawImage,rawImage+(params.imageSizeY*params.imageSizeX));

    double* intermediateImage = new double[(params.imageSizeY*params.imageSizeX)];
    iStretchIm(intermediateImage, rawImage, theMin, theMax, params);
    intermediateImage = myConv2Same(intermediateImage, params.imageSizeY, params.imageSizeX);
    iReplacetoExtIm(processedImage, intermediateImage, params);
    delete[] intermediateImage; //input image no longer needed
}

double *myConv2Same(double* ar, int dimX, int dimY) //Performs convolution using a 3 by 3 separable Gaussian mask
{
    double mask[] = {0.25, 0.5, 0.25};
    int maskLength = 3;
    int convSizeY = dimY+maskLength-1;
    int convSizeX = dimX+maskLength-1;
    double* resultOne = new double[(dimX*convSizeY)];
    fill_n(resultOne,dimX*convSizeY,0);
    for(int k=0; k < dimX; k++)
        for(int i=0; i < convSizeY; i++)
            for(int j = max(i-(maskLength-1),0);j <= min(i,dimY-1); j++)
                resultOne[sub2Dind(k,i,convSizeY)] += ar[sub2Dind(k,j,dimY)]*mask[i-j]; //do convolution in one direction

    double* resultTwo = new double[(convSizeX*convSizeY)];
    fill_n(resultTwo,convSizeX*convSizeY,0);
    for(int k=0; k < convSizeY;k++)
        for(int i=0; i < convSizeX;i++)
            for(int j = max(i-(maskLength-1),0);j <= min(i,dimX-1); j++)
                resultTwo[sub2Dind(i,k,convSizeY)] += resultOne[sub2Dind(j,k,convSizeY)]*mask[i-j]; //do convolution in the other direction

    double* resultFinal = new double[(dimX*dimY)];
    fill_n(resultFinal,dimX*dimY,0);
    for(int i=0; i < dimX; i++)
        for(int j=0; j<dimY;j++)
            resultFinal[sub2Dind(i,j,dimY)] = resultTwo[sub2Dind(i+1,j+1,convSizeY)]; //copy the result into the output (keeping the center portion and discarding the rest)

    delete[] ar;
    delete[] resultOne;
    delete[] resultTwo;
    return resultFinal;
}

/* The maximum grey-value in outputIm will be set to maxGrayLevel,
** the minimum greyvalue min to minGrayLevel. The other grey-values will be rescaled accordingly. */
void iStretchIm(double* outputIm, double* inputIm, double &min, double &max, struct PreProcessParams params)
{
    #pragma omp simd
    for(int i=0; i < params.imageSizeY*params.imageSizeX; i++)
        outputIm[i] = params.minGrayLevel + (inputIm[i] - min)/(max - min)*(params.maxGrayLevel - params.minGrayLevel);
}

//This function copies midImage into outImage
void iReplacetoExtIm(double* outImage, double* midImage, struct PreProcessParams params)
{
    int difh;
    difh = (params.extendedSizeX-params.imageSizeX)/2;
    for(int i=0; i < params.imageSizeY; i++)//Filling the center part
        for(int j=0; j < params.imageSizeX; j++)
            outImage[sub2Dind(i+difh,j+difh,params.extendedSizeX)] = midImage[sub2Dind(i,j,params.imageSizeX)];
}
//This function is one of 3 helper functions that allow MATLAB style multi-dimensional array indexing
//These functions are used extensively and should be inline'd by GCC when using O2 or above
//to use this function with a 2D array you write "array2D[sub2Dind(i,j,numColumns)]" equivalent to writing "array2D(i,j)" in MATLAB
int sub2Dind(int xLoc, int yLoc, int yDim)
{
    return xLoc*yDim + yLoc; //columns fastest running variable. That is consecutive array elements traverse across each row
}
//This function is one of 3 helper functions that allow MATLAB style multi-dimensional array indexing
//These functions are used extensively and should be inline'd by GCC when using O2 or above
//to use this function with a 3D array you write "array3D[sub3Dind(i,j,k, numRows, numColumns)]" equivalent to writing "array3D(i,j,k)" in MATLAB
int sub3Dind(int xLoc, int yLoc, int zLoc, int xDim, int yDim)
{
    return zLoc*xDim*yDim + sub2Dind(xLoc,yLoc,yDim); //columns fastest running variable
}
//This function is one of 3 helper functions that allow MATLAB style multi-dimensional array indexing
//These functions are used extensively and should be inline'd by GCC when using O2 or above
//to use this function with a 4D array you write "array4D[sub4Dind(i,j,k,l, numRows, numColumns, numSlices)]" equivalent to writing "array4D(i,j,k,l)" in MATLAB
int sub4Dind(int xLoc, int yLoc, int zLoc, int wLoc, int xDim, int yDim, int zDim)
{
    return wLoc*xDim*yDim*zDim + sub3Dind(xLoc,yLoc,zLoc,xDim,yDim); //columns fastest running variable
}
