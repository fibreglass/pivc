CC=g++
CFLAGS=-I.
SRC=workspace.cpp preprocess.cpp dowholefield.cpp verifyfieldgrid.cpp filemanip.cpp PIVWriter.cpp inputParser.c
OSRC=workspace.o preprocess.o dowholefield.o verifyfieldgrid.o filemanip.o PIVWriter.o inputParser.o
LIBS=-s -lfftw3 -lgomp -ltiff -lm -lz
PREFLAGS=-Wall -fexceptions -Wno-unknown-pragmas -march=native -mtune=native -O3 -fopenmp -m64 -std=c++20 
GCCFLAGS=-Wall -fexceptions -Wno-unknown-pragmas -march=native -mtune=native -O3 -fopenmp -m64

SRCTEST=pivctester.cpp preprocess.cpp dowholefield.cpp verifyfieldgrid.cpp filemanip.cpp
LIBSTEST=-lfftw3 -ltiff -lm -lz
PREFLAGSTEST=-Wall -fexceptions -Wno-unknown-pragmas -std=c++20 -m64

PIVC: $(SRC)
	$(CC) -o ./dowholefield.o -c ./dowholefield.cpp $(PREFLAGS) 
	$(CC) -o ./filemanip.o -c ./filemanip.cpp $(PREFLAGS) 
	gcc -o ./inputParser.o -c ./inputParser.c $(GCCFLAGS) 
	$(CC) -o ./PIVWriter.o -c ./PIVWriter.cpp $(PREFLAGS) 
	$(CC) -o ./preprocess.o -c ./preprocess.cpp $(PREFLAGS) 
	$(CC) -o ./verifyfieldgrid.o -c ./verifyfieldgrid.cpp $(PREFLAGS) 
	$(CC) -o ./workspace.o -c ./workspace.cpp $(PREFLAGS)
	$(CC) -o PIVC $(OSRC) $(LIBS)
	@echo Cleaning Up
	@rm -rf ./*.o
	@echo Build Complete

PIVC-TEST: $(SRCTEST)
	$(CC) -o PIVC-TEST $(SRCTEST) $(CFLAGS) $(PREFLAGSTEST) $(LIBSTEST)	

.PHONY: test
test: PIVC-TEST	
	./PIVC-TEST

.PHONY: clean-test
clean-test: 
	@rm -f ./PIVC-TEST
	@rm -rf ./test/results/*

.PHONY: clean
clean:
	@rm -f ./PIVC-TEST
	@rm -f ./PIVC
	@rm -rf ./*.o
	@rm -rf ./test/results/*