#ifndef FILEMANIP_H
#define FILEMANIP_H

#include <iostream>
#include <string>
#include <tiffio.h>
#include <fstream>
#include <filesystem>
#include <vector>

using namespace std;
namespace fs = filesystem;

double* myTiffRead8Bit(TIFF* tif, uint32 &sizeX, uint32 &sizeY);
void getFiles(vector<string> &fileList, string myDir, int &numFiles);
double* myTiffRead16Bit(TIFF* tif, uint32 &sizeX, uint32 &sizeY);
double* myTiffRead(string fileName, uint32 &sizeX, uint32 &sizeY);
double* myTiffReadOld(string fileName, uint32 &sizeX, uint32 &sizeY);

#endif // FILEMANIP_H
