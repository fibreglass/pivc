#include "verifyfieldgrid.h"
#include "preprocess.h"

using namespace std;
//Finalizing the vectorfield is performed by VerifyFieldGrid
//Input: peaks -- 4D array containing the cross-correlation peaks and pixel shifts of each vector
//Input: params -- processing parameters such as the image sizes, window sizes, and etc.
//Return: field -- 3D array containing finalized vectors
void VerifyFieldGrid(double* field, double* peaks, struct VectorFinalParams params)
{
    int mareas = params.meanAreaSize;
    int fielddims[] =  {params.yGrids,params.xGrids,3,3}; //This is the size of the input 4D array, peaks.
    double *extfield;
    int ySize = (params.yGrids + mareas - 1); //dimensions of extfield
    int xSize = (params.xGrids + mareas - 1);
    extfield = new double[ySize*xSize*2];
    fill_n(extfield,ySize*xSize*2,0);
    iFlipExtendVField(extfield, peaks, fielddims, mareas-1); //copy peaks into extfield while re-organizing contents
    iVerifyFieldGrid(field, extfield, peaks, fielddims, mareas, params.peakThreshold, params.offsetx, params.offsety, params.gridSpacing, params.resetVal); //perform the final analysis of the vectors
    delete[] extfield;
}

//This function finalizes the computed vector field (field)
//It returns, d, the finalized 3D vector field
void iVerifyFieldGrid(double* d, double *extfield, double *field, int *fields, int &mareas, double &threshold, double &offsetx, double &offsety, int &griddis, double &resetval)
{
    int extFieldSize[] = {(fields[0] + mareas - 1),(fields[1] + mareas - 1)};
    int mareaproduct = mareas*mareas;
    int midindex = mareaproduct/2+1;
    int startindex = mareaproduct/4;

    double *marea = new double[mareaproduct*2];
    fill_n(marea,mareaproduct*2,0);
    double *xval = new double[mareaproduct];
    double *yval = new double[mareaproduct];
    int selected;
    double dif1, dif2;
    double mean[] = {0,0};

    for(int i = 0; i < fields[0]; i++)
    {
        for(int j = 0; j < fields[1]; j++)
        {
            for(int k = 0; k < mareas; k++) //First we test the accuracy of each vector (remember there are 3 per grid point thus far) in its surrounding 3 by 3 region using the central mean method
                for(int l = 0; l < mareas; l++)
                    for(int n = 0; n < 2; n++)
                        marea[sub3Dind(k,l,n,mareas,mareas)] = extfield[sub3Dind(i+k,j+l,n,extFieldSize[0],extFieldSize[1])]; //Copy section of extfield into the current region

            CentralMean(mean,marea,mareaproduct,midindex,startindex,xval,yval); //calculate the central mean (average pixel shift) of the vectors surrounding the current one
            selected = 0;
            dif1 = sqrt(pow(mean[0] - field[sub4Dind(i,j,0,0,fields[0],fields[1],3)],2)+pow(mean[1] - field[sub4Dind(i,j,0,1,fields[0],fields[1],3)],2)); //calculate the difference from the central mean

            if(dif1 > threshold)
            {					// find best peak -- the vector with the smallest difference from the central mean while still being greater than threshold is selected
                for(int k = 1; k < fields[2]; k++)
                {
                    dif2 = sqrt(pow(mean[0] - field[sub4Dind(i,j,k,0,fields[0],fields[1],3)],2)+pow(mean[1] - field[sub4Dind(i,j,k,1,fields[0],fields[1],3)],2));
                    if(dif2 < dif1)
                    {
                        selected = k;
                        dif1 = dif2;
                    }
                }
            }
            d[sub3Dind(i,j,2,fields[0],fields[1])] = field[sub4Dind(i,j,selected,0,fields[0],fields[1],3)];			// setting the best values (pixel shifts of the selected vector)
            d[sub3Dind(i,j,3,fields[0],fields[1])] = field[sub4Dind(i,j,selected,1,fields[0],fields[1],3)];
            d[sub3Dind(i,j,4,fields[0],fields[1])] = dif1;								// deviation from the central mean
            d[sub3Dind(i,j,5,fields[0],fields[1])] = field[sub4Dind(i,j,selected,2,fields[0],fields[1],3)]/field[sub4Dind(i,j,1,2,fields[0],fields[1],3)]; // signal-to-noise
            d[sub3Dind(i,j,8,fields[0],fields[1])] = offsetx + j*griddis;				// grid positions
            d[sub3Dind(i,j,9,fields[0],fields[1])] = offsety + i*griddis;
            d[sub3Dind(i,j,0,fields[0],fields[1])] = d[sub3Dind(i,j,8,fields[0],fields[1])] + d[sub3Dind(i,j,2,fields[0],fields[1])]/2.0;		// real vector position with sub-pixel accuracy
            d[sub3Dind(i,j,1,fields[0],fields[1])] = d[sub3Dind(i,j,9,fields[0],fields[1])] + d[sub3Dind(i,j,3,fields[0],fields[1])]/2.0;
            if(field[sub4Dind(i,j,selected,2,fields[0],fields[1],3)] > d[sub3Dind(i,j,6,fields[0],fields[1])])
            {		// mark vectors where peak was found (see iMax3 in dowholefield) in the output file
                d[sub3Dind(i,j,6,fields[0],fields[1])] = field[sub4Dind(i,j,selected,2,fields[0],fields[1],3)];
                d[sub3Dind(i,j,7,fields[0],fields[1])] = resetval;
            }
            else //mark vectors where NO peak was found in the output file
                if(d[sub3Dind(i,j,7,fields[0],fields[1])] > 0.01)
                    d[sub3Dind(i,j,7,fields[0],fields[1])]--;
        }
    }
    delete[] marea;
    delete[] xval;
    delete[] yval;
}

//This function computes the central mean of vectors in velmatrix
//It returns the central mean of each vector component
void CentralMean(double* d, double* velmatrix, int &number, int &mid, int &start, double* xval, double* yval)
{
    //The central mean is simply the average of the "middle" vectors when sorted from smallest to largest by magnitude
    //int mid is the the middle index
    //int start; //Look equally on both sides of the middle index by "start" elements and that is the middle
    double meanx = 0;
    double meany = 0;
    copy_n(velmatrix,number,xval);
    copy_n(velmatrix+number,number,yval);
    sort(xval,xval+number); //sort the vectors by magnitude
    sort(yval,yval+number);

    for(int i = start; i < mid+start; i++) //calculate the average
    {
        meanx += xval[i];
        meany += yval[i];
    }
    d[0] = meanx/mid;
    d[1] = meany/mid;
}

//This function copies peaks (field) into the output variable (extfield)
//It also flips peaks left and right and then flips peaks top and bottom
void iFlipExtendVField(double *extfield, double *field, int* fields, int dif)
{
    int difh = dif/2;
    int extFieldSize[] = {(fields[0] + dif),(fields[1] + dif)};

    for(int i = 0; i < fields[0]; i++)		// filling the centre
        for(int j = 0; j < fields[1]; j++)
            for(int k = 0; k < 2; k++)
                extfield[sub3Dind(i+difh,j+difh,k,extFieldSize[0],extFieldSize[1])] = field[sub4Dind(i,j,0,k,fields[0],fields[1],3)];

    for(int i = 0; i < difh; i++)			// flipping left and right
        for(int j = 0; j < fields[0]; j++)
            for(int k = 0; k < 2; k++)
            {
                extfield[sub3Dind(j+difh,i,k,extFieldSize[0],extFieldSize[1])] = field[sub4Dind(j,difh-1-i,0,k,fields[0],fields[1],3)];
                extfield[sub3Dind(j+difh,i+fields[1]+difh,k,extFieldSize[0],extFieldSize[1])] = field[sub4Dind(j,fields[1]-1-i,0,k,fields[0],fields[1],3)];
            }

    for(int i = 0; i < dif/2; i++)			// flipping top and bottom
        for(int j = 0; j < fields[1]+dif; j++)
            for(int k = 0; k < 2; k++)
            {
                extfield[sub3Dind(i,j,k,extFieldSize[0],extFieldSize[1])] = extfield[sub3Dind(dif-1-i,j,k,extFieldSize[0],extFieldSize[1])];
                extfield[sub3Dind(i+fields[0]+difh,j,k,extFieldSize[0],extFieldSize[1])] = extfield[sub3Dind(fields[0]+difh-1-i,j,k,extFieldSize[0],extFieldSize[1])];
            }
}
