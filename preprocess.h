#ifndef PREPROCESS_H
#define PREPROCESS_H

#include <omp.h>
#include <algorithm>
#include <math.h>
#include <vector>

struct PreProcessParams { //Parameters for the preprocessing
    int minGrayLevel; //These parameters are used in the level operation (brightness/contrast adjustment in preprocessing)
    int maxGrayLevel; //These parameters are used in the level operation (brightness/contrast adjustment in preprocessing)
    int imageSizeY;
    int imageSizeX;
    int extendedSizeY;
    int extendedSizeX;
};

using namespace std;

void preProcessImage(double* processedImage, double* rawImage, struct PreProcessParams params);
void iStretchIm(double* outputIm, double* inputIm, double &min, double &max, struct PreProcessParams params);
void iReplacetoExtIm(double* outImage, double* midImage, struct PreProcessParams params);
double *myConv2Same(double* ar, int dimX, int dimY);
int sub2Dind(int xLoc, int yLoc, int yDim);
int sub3Dind(int xLoc, int yLoc, int zLoc, int xDim, int yDim);
int sub4Dind(int xLoc, int yLoc, int zLoc, int wLoc, int xDim, int yDim, int zDim);

#endif // PREPROCESS_H
