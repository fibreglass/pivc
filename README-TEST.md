# Introduction

Welcome to the PIVC Tester. This is a basic functionality and accuracy tester for PIVC. 

## How to use the PIVC tester

Using the PIVC tester is very straight forward at a high level. 

* Meet all dependencies for compiling PIVC
* Navigate to the source code directory then run 

```sh
make test
```

This will compile PIVC-TEST which is a stand alone program that tests the implementation of PIVC stored locally. By running "make test", the compiled tester will automatically execute and write results for each major step in the PIVC analysis in text files stored in

```sh
./test/results/
```
The steps of the analysis algorithm being tested are 

* Preprocessing
* Vector Computation (output of the cross-correlation)
* Vector Finalization (verification of computed vectors using a central mean (i.e. mean of adjacent vectors) test).

The test compares the output of the current PIVC implementation to a known correct output at each major step, hence PIVC-TEST performs 3 independent tests. The test colection is performed for a 320 by 240 sample image pair which is stored in "./test/images-preprocess". Tests are performed using a grid spacing of 16 pixels. 

In the event there are discrepancies (greater than 1E-6 for any computed quantity) found in any of the tests, the test result files will contain the index of the incorrect element and the difference between the expected value and the value output by the current PIVC implementation. This information is to assist developers in identifying bugs. 

## Contents of the test folders

Inside each of the "./test/target-\*" folders are the known solutions used for testing. Similarly inside each of the "./test/images-\*" folders is known good input data for each part of the testing process. Preprocessing in PIVC works on both images in the image pair. The text files "imageOne.txt" and "imageTwo.txt" in the "./test/target-preprocess" folder contain the known correct answers for the preprocessing step. The output of preprocessing is an image. Each entry in these files contains the gray-level value of each pixel in the corresponding image. The text file "peaks.txt" in the "./test/target-veccomp" folder contains the known correct answers for the vector computation portion of the analysis. Each row in the "peaks.txt" file contains the following information

* Row 1: Horizontal (x) pixel shift of vector 1 
* Row 2: Horizontal (x) pixel shift of vector 2 
* Row 3: Horizontal (x) pixel shift of vector 3
* Row 4: Vertical (y) pixel shift of vector 1 
* Row 5: Vertical (y) pixel shift of vector 2
* Row 6: Vertical (y) pixel shift of vector 3
* Row 7: Cross-correlation magnitude for vector 1
* Row 8: Cross-correlation magnitude for vector 2
* Row 9: Cross-correlation magnitude for vector 3

where each column correspeds to a unique location in the image. The text file "velocityfield.txt" in the "./test/target-vecfinal" folder contains the known correct answers for the vector finalization portion of the analysis. Each row in the "velocityfield.txt" file contains the following information. 

* Row 1: Subpixel location of vector (x)
* Row 2: Subpixel location of vector (y)
* Row 3: Horizontal pixel shift (x-direction)
* Row 4: Vertical pixel shift (y-direction)
* Row 5: Deviation from central mean
* Row 6: Signal-to-noise ratio
* Row 7: The highest so far achieved correlation peak height
* Row 8: A counter value
* Row 9: Horizontal coordinate of vector (x) on regular grid
* Row 10: Vertical coordinate of vector (y) on regular grid

Note that PIVC internally uses image processing coordinates for all locations. 
