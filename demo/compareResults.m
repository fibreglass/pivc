%This script compares the output of PIVC computed velocity fields to the exact solutions in synthetic images
%The comparison is performed statistically based on the median velocity of each velocity component 
%of the exact solution in the corresponding PIV interrogation window. 
%Detailed results of the comparison are written to the MATLAB file containing the exact solution
%A short summary of results in written to the MATLAB terminal

%In order to use this script, the directory of the exact solutions, image sizes, and PIVC grid spacing must all be specified.
%In addition, the output of PIVC must be written to a folder named "VortexPIVCOutput" and "ShearPIVCOutput" in the same directory as each respective solution file.

clear; 
clc;
vortexfolder = '%% FULL PATH TO THE VORTEX SOLUTIONS FILE %% ';
shearfolder = '%% FULL PATH TO THE SHEAR SOLUTIONS FILE %%';
gridSpacing = 12; %grid spacing used in PIVC
imageSize = 500; %size of synthetic images

%Point vortex results comparison
compareResultsVortex(vortexfolder,gridSpacing,imageSize);
load(strcat(vortexfolder,'vortexSolution.mat'),'results');
bulkdata = [];
for(i=1:length(results))
    bulkdata = cat(1,bulkdata,[results{i}(:,7),results{i}(:,8)]);
end
fprintf("Point vortex flow results \n");
fprintf("%d vectors computed \n",length(bulkdata));
fprintf("Median difference in X-direction velocity component was %0.2f percent \n",100*median(bulkdata(:,1)));
fprintf("Median difference in Y-direction velocity component was %0.2f percent \n",100*median(bulkdata(:,2)));
fprintf("\n");

clear bulkdata results;

%Point vortex in couette flow comparison
compareResultsShear(shearfolder,gridSpacing,imageSize);
load(strcat(shearfolder,'shearSolution.mat'),'results');
bulkdata = [];
for(i=1:length(results))
    bulkdata = cat(1,bulkdata,[results{i}(:,7),results{i}(:,8)]);
end
fprintf("Point vortex with Couette flow results \n");
fprintf("%d vectors computed \n",length(bulkdata));
fprintf("Median difference in X-direction velocity component was %0.2f percent \n",100*median(bulkdata(:,1)));
fprintf("Median difference in Y-direction velocity component was %0.2f percent \n",100*median(bulkdata(:,2)));

function compareResultsVortex(folder,gridSize,imSize)    
    content = dir(strcat(folder,'VortexPIVCOutput/')); %Get a list of all files
    fileList = {content(~[content.isdir]).name}; %Store list here
    numFiles = length(fileList);    %Get the number of files
    load(strcat(folder,'vortexSolution.mat'));    
    
    vData = -1*vData; %conversion from image processing to cartesian coordinates
    results = cell(numFiles,1);
    for(k=1:numFiles)
        data=load(strcat(folder,'VortexPIVCOutput/',fileList{k})); 
        dataCompare = zeros(size(data,1),6);
        dataCompare(:,1:2) = round(data(:,9:10)); %Rounding is used on PIVC results in order to place computed PIVC vectors on integer coordinates 
        dataCompare(:,3:4) = data(:,3:4);
        for(i=1:size(dataCompare,1)) %For all vectors computed in PIVC
            %Get the exact solution velocity field for each velocity component corresponding to the current PIV interrogation window
            uSample = uData(max([0,dataCompare(i,2)-gridSize]):min([imSize,dataCompare(i,2)+gridSize]),max([0,dataCompare(i,1)-gridSize]):min([imSize,dataCompare(i,1)+gridSize]),k);
            vSample = vData(max([0,dataCompare(i,2)-gridSize]):min([imSize,dataCompare(i,2)+gridSize]),max([0,dataCompare(i,1)-gridSize]):min([imSize,dataCompare(i,1)+gridSize]),k);
            dataCompare(i,5) = median(uSample,'all'); %compare the median velocity of the exact solution for each velocity component
            dataCompare(i,6) = median(vSample,'all');
        end
        dataCompare(:,7) = abs(dataCompare(:,3) - dataCompare(:,5))./abs(dataCompare(:,5));
        dataCompare(:,8) = abs(dataCompare(:,4) - dataCompare(:,6))./abs(dataCompare(:,6));
        results{k} = dataCompare;
    end  
    save(strcat(folder,'vortexSolution.mat'),"results",'-append');
end

function compareResultsShear(folder,gridSize,imSize)    
    content = dir(strcat(folder,'ShearPIVCOutput/')); %Get a list of all files
    fileList = {content(~[content.isdir]).name}; %Store list here
    numFiles = length(fileList);    %Get the number of files
    load(strcat(folder,'shearSolution.mat'));    
    
    vData = -1*vData; %conversion from image processing to cartesian coordinates
    results = cell(numFiles,1);
    for(k=1:numFiles)
        data=load(strcat(folder,'ShearPIVCOutput/',fileList{k})); 
        dataCompare = zeros(size(data,1),6);
        dataCompare(:,1:2) = round(data(:,9:10)); %Rounding is used on PIVC results in order to place computed PIVC vectors on integer coordinates 
        dataCompare(:,3:4) = data(:,3:4);
        for(i=1:size(dataCompare,1)) %For all vectors computed in PIVC
            %Get the exact solution velocity field for each velocity component corresponding to the current PIV interrogation window
            uSample = uData(max([0,dataCompare(i,2)-gridSize]):min([imSize,dataCompare(i,2)+gridSize]),max([0,dataCompare(i,1)-gridSize]):min([imSize,dataCompare(i,1)+gridSize]),k);
            vSample = vData(max([0,dataCompare(i,2)-gridSize]):min([imSize,dataCompare(i,2)+gridSize]),max([0,dataCompare(i,1)-gridSize]):min([imSize,dataCompare(i,1)+gridSize]),k);
            dataCompare(i,5) = median(uSample,'all'); %compare the median velocity of the exact solution for each velocity component
            dataCompare(i,6) = median(vSample,'all');
        end
        dataCompare(:,7) = abs(dataCompare(:,3) - dataCompare(:,5))./abs(dataCompare(:,5));
        dataCompare(:,8) = abs(dataCompare(:,4) - dataCompare(:,6))./abs(dataCompare(:,6));
        results{k} = dataCompare;
    end  
    save(strcat(folder,'shearSolution.mat'),"results",'-append');
end