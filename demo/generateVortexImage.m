%This script simulates PIV experiments on stationary velocity fields of the point vortex:
%the same velocity field is used to generate all particle image pairs
%Particle image pairs differ for the position of particles (random)
%This code is adapted from the repo located at https://github.com/lu-p/standard-PIV-image-generator
%Input: vMagnitude -- maximum velocity magnitude produced by the point vortex
%Input: imagesizeX and imagesizeY -- output image dimensions
%Output: imageA and imageB -- dimensions of generated synthetic PIV images
%Output: ufin and vfin -- the exact x-directed (ufin) and y-directed (vfin) velocities fields

function [imageA,imageB,ufin,vfin]=generateVortexImage(vMagnitude,imagesizeX,imagesizeY)

    n_imagepairs=1; % number of particle image pairs
    dec=4; % significant decimals
    extrapx=4; % number of pixels beyond each boundary (particles are allowed to leave/enter the camera field of view)

    resx=imagesizeX; % image resolution x [px]
    resy=imagesizeY; % image resolution along y [px]
    xmax=2*extrapx+resx; % image resolution along x before cropping
    ymax=2*extrapx+resy; % image resolution along y before cropping

    xvf=-xmax/2+0.5:1:xmax/2-0.5; % pixels along x (x axis: sx->dx)
    yvf=ymax/2-0.5:-1:-ymax/2+0.5;% pixels along y (y axis: down->top)
    [XVF, YVF]=meshgrid(xvf, yvf);
    
    rho=0.029; %[ppp] 
    n_particles=round(rho*xmax*ymax);% number of particles before cropping
    
    Ifin1=zeros(n_imagepairs,resy,resx); % first exposition
    Ifin2=zeros(n_imagepairs,resy,resx); % second exposition

    %% 2D VELOCITY FIELD: RANKINE VORTEX
    
    % vortex (central vortex)
    v0=vMagnitude;%-4+8*rand; %max velocity
    [x,y]=meshgrid(xvf,yvf);
    [o,r]=cart2pol(x,y);
    R1=100+min(xmax/3,ymax/3)*rand; %radius
    uoin = (r <= R1);
    uout = (r > R1);
    uo = uoin+uout;
    uo(uoin) =  v0*r(uoin)/R1;
    uo(uout) =  v0*R1./r(uout);
    uo(isnan(uo))=0;
    u = -uo.*sin(o);
    v = uo.*cos(o);
    ufin=imcrop(u,[extrapx+1,extrapx+1,resx-1,resy-1]);
    vfin=imcrop(v,[extrapx+1,extrapx+1,resx-1,resy-1]);

    h=0.5; % half time step
    
    div_f=255; % div_f=255 normalizes images in [0, 1] (real numbers)
    
    I0=randi(56,[n_particles,n_imagepairs])+199; % peak intensity % [grey value] from 200 to 255
    dp=1.5+2.5*rand(n_particles,n_imagepairs);% effective particle image diameter [px]
        
    for nip=1:n_imagepairs        
        I1=zeros(ymax,xmax);
        I2=zeros(ymax,xmax);
        
        % LOCATION OF PARTICLE CENTRES
        
        r0=[xmax/2*(-1 + 2*rand(n_particles,1)), ymax/2*(-1+2*rand(n_particles,1))];% initial particle centres        
    
        %% INTEGRATION OVER TIME (multiplication by time interval)
        [o,r]=cart2pol(r0(:,1),r0(:,2));
        uoin = (r <= R1);
        uout = (r > R1);
        uo = uoin+uout;
        uo(uoin) =  v0*r(uoin)/R1;
        uo(uout) =  v0*R1./r(uout);
        uo(isnan(uo))=0;
        uu = -uo.*sin(o);%by pivlab -
        vv = uo.*cos(o);%by pivlab +
        
        r_uv12=[r0 r0]+h*[-uu, -vv, uu, vv]; % particle centres [r1u r1v r2u r2v]
        
        for np=1:n_particles        
           I1=I1+I0(np,nip)*exp((-(XVF-r_uv12(np,1)).^2-(YVF-r_uv12(np,2)).^2)/(1/8*dp(np,nip)^2));
           I2=I2+I0(np,nip)*exp((-(XVF-r_uv12(np,3)).^2-(YVF-r_uv12(np,4)).^2)/(1/8*dp(np,nip)^2));        
        end        
       
        I1=imcrop(I1,[extrapx+1,extrapx+1,resx-1,resy-1]); % [XMIN YMIN WIDTH HEIGHT]
        I2=imcrop(I2,[extrapx+1,extrapx+1,resx-1,resy-1]); % [XMIN YMIN WIDTH HEIGHT]
        
        I1(I1>255)=255;
        I2(I2>255)=255;
        
        I1=round(I1/div_f, dec);
        I2=round(I2/div_f, dec);
        
        Ifin1(nip,:,:)=I1+imnoise(I1,'gaussian',0,max(max(I1))/100);
        Ifin2(nip,:,:)=I2+imnoise(I2,'gaussian',0,max(max(I2))/100);        
  
    end
    imageA = mat2gray(squeeze(Ifin1(1,:,:)));
    imageB = mat2gray(squeeze(Ifin2(1,:,:)));
end