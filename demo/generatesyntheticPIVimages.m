%This function generates synthetic PIV images of a point vortex (rankine vortex) 
%and point vortex in uni-directional shear flow (Couette flow).
%Input: imagesize -- specifies the size of synethic images. All generated images are square hence only one dimension is needed.
%Input: numFields -- specifies the number of vector fields to generate per flow condition. Max 498 vector fields per flow condition. 
%Output: Images are written to current directory in folders named VortexImages and ShearImages.
%Output: In addition to images, the exact 2D velocity field is also stored in at MATLAB file within the respective folders.
function generatesyntheticPIVimages(imageSize, numFields)
    maxVel = -10*rand(numFields,1)+5;  
    maxVel((maxVel<1) & (maxVel>0))=maxVel((maxVel<1) & (maxVel>0)) + 1;
    maxVel((maxVel>-1) & (maxVel<0))=maxVel((maxVel>-1) & (maxVel<0)) - 1;
    uData = zeros(imagesize,imagesize,numFields);
    vData = zeros(imagesize,imagesize,numFields);
    for(i=1:numFields)
        [vortexA,vortexB,uData(:,:,i),vData(:,:,i)]=generateVortexImage(maxVel(i),imagesize,imagesize); %vortex images
        imwrite(vortexA,strcat(pwd,sprintf('/VortexImages/vortex_%03d.tif',2*i-1)));
        imwrite(vortexB,strcat(pwd,sprintf('/VortexImages/vortex_%03d.tif',2*i)));
    end
    save(strcat(pwd,'/vortexSolution.mat'),"uData","vData");

    clear vortexA vortexB uData vData maxVel;
 
    shearMagnitude = sign(rand(numFields,1)-0.5)*0.01;
    maxVel = -10*rand(numFields,1)+5;  
    maxVel((maxVel<1) & (maxVel>0))=maxVel((maxVel<1) & (maxVel>0)) + 1;
    maxVel((maxVel>-1) & (maxVel<0))=maxVel((maxVel>-1) & (maxVel<0)) - 1;
    uData = zeros(imagesize,imagesize,numFields);
    vData = zeros(imagesize,imagesize,numFields);
    for(i=1:numFields)
        [shearA,shearB,uData(:,:,i),vData(:,:,i)]=generateVortexShearImage(maxVel(i),shearMagnitude(i),imagesize,imagesize);
        imwrite(shearA,strcat(pwd,sprintf('/ShearImages/shear_%03d.tif',2*i-1)));
        imwrite(shearB,strcat(pwd,sprintf('/ShearImages/shear_%03d.tif',2*i)));
    end
    save(strcat(pwd,'/shearSolution.mat'),"uData","vData");  
end