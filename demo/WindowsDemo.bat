@echo off
echo This a demonstration of PIVC in multiple flow configurations.
echo All analysis in this demo is performed using the multi-threaded version of PIVC.
PAUSE
echo.

echo Now Processing: Boundary Layer Flow Image Demo
mkdir -p "demo/BL output"
PIVC.exe -g 16 -c plain -i "%CD%/demo/BL images" -o "%CD%/demo/BL output/BLdataPIVC" -s imageproc
echo Boundary Layer Flow analysis complete.
PAUSE
echo.

echo Now Processing: Moth Wake Flow Image Demo
mkdir -p "demo/moth output"
PIVC.exe -g 32 -c plain -i "%CD%/demo/moth images" -o "%CD%/demo/moth output/mothdataPIVC" -s imageproc
echo Moth Wake Flow analysis complete.
PAUSE
echo.

echo Now Processing: Point Vortex Flow Image Demo
mkdir -p "demo/VortexPIVCOutput"
PIVC.exe -g 12 -c plain -i "%CD%/demo/vortex images" -o "%CD%/demo/VortexPIVCOutput/vortexdataPIVC" -s imageproc
echo Point Vortex Flow analysis complete.
PAUSE
echo.

echo Now Processing: Point Vortex in Couette Flow Image Demo
mkdir -p "demo/ShearPIVCOutput"
PIVC.exe -g 12 -c plain -i "%CD%/demo/shear images" -o "%CD%/demo/ShearPIVCOutput/sheardataPIVC" -s imageproc
echo Point Vortex in Couette Flow analysis complete.
echo.

echo PIVC demonstration complete
PAUSE