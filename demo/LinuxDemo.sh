echo "This a demonstration of PIVC in multiple flow configurations."
echo "All analysis in this demo is performed using the multi-threaded version of PIVC."
read -p "Press enter to continue"

echo ""
echo "Now Processing: Boundary Layer Flow Image Demo"
mkdir -p "demo/BL output"
./PIVC -g 16 -c plain -i "./demo/BL images" -o "./demo/BL output/BLdataPIVC" -s imageproc
echo "Boundary Layer Flow analysis complete."
read -p "Press enter to continue"
echo ""

echo "Now Processing: Moth Wake Flow Image Demo"
mkdir -p "demo/moth output"
./PIVC -g 32 -c plain -i "./demo/moth images" -o "./demo/moth output/mothdataPIVC" -s imageproc
echo "Moth Wake Flow analysis complete."
read -p "Press enter to continue"
echo ""

echo "Now Processing: Point Vortex Flow Image Demo"
mkdir -p "demo/VortexPIVCOutput"
./PIVC -g 12 -c plain -i "./demo/vortex images" -o "./demo/VortexPIVCOutput/vortexdataPIVC" -s imageproc
echo "Point Vortex Flow analysis complete."
read -p "Press enter to continue"
echo ""

echo "Now Processing: Point Vortex in Couette Flow Image Demo"
mkdir -p "demo/ShearPIVCOutput"
./PIVC -g 12 -c plain -i "./demo/shear images" -o "./demo/ShearPIVCOutput/sheardataPIVC" -s imageproc
echo "Point Vortex in Couette Flow analysis complete."
echo ""

echo "PIVC demonstration complete"