#ifndef DOWHOLEFIELD_H
#define DOWHOLEFIELD_H

#include <fftw3.h>

struct VectorCompParams { //parameters for vector computation
    int interWindowSize; //interrogation window size
    int searchWindowSize; //search window size
    int gridSpacing; //grid spacing
    int extendedSizeX; //extended image size
    int xGrids; //number of vectors in x direction
    int yGrids; //number of vectors in y direction
    double offsetx; //x direction offset
    double offsety; //y direction offset
};

void DoWholeField(double *peaks, double *imageA, double *imageB, struct VectorCompParams params);
void DetDisp(double* v, double* ipip, double* spip, int &nspip, int &nipip);
void CrossCorFft(double *matrix, int &msize, double *ipipAr, int &isize, double *spipAr, int &ssize);
void iExtrMatr(double *matrix, double *result, int &msize, int &fftsize);
int NextPof2(int &number);
void iSubCopyInAoi(double *ipip, double &nfactor, double *iext, int &offset, int &isize, int &extsize);
void iCopyAoiInIP(double *im, double *pos, double *pip, int pips, int sws);

void iMax3(double *d, double *cmatrix, int &msize);
void SortPeaks(double *d, double peaks[3][3]);
void SubPixDet(double &d0, double &d1, int x, int y, double gv00, double gv0m1,double gvm10, double gv0p1,double gvp10);

fftw_complex* addConj(fftw_complex *vectorA, fftw_complex *vectorB, int &sizeX, int &sizeY);
double* reshape2DFFT(double* inArray, int &sizeY, int &sizeX);
double imageMean(double *image, int length);

#endif // DOWHOLEFIELD_H
