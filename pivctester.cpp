#include "filemanip.h"
#include "preprocess.h"
#include "dowholefield.h"
#include "verifyfieldgrid.h"
#include <cmath>

void testPreProcess(int grid);
void loadTextData(string filename, double* data);
void testVectorComp(int gridSpacing);
void testVectorFinal(int gridSpacing);

int main()
{
    int grid = 16;
    cout<<"Now performing tests on PIVC."<<endl;
    cout<<"Preprocessing Test"<<endl;
    testPreProcess(grid);
    cout<<"Vector Computation Test"<<endl;
    testVectorComp(grid);
    cout<<"Vector Finalization Test"<<endl;
    testVectorFinal(grid);
    cout<<"All tests complete. Check results folder for all test results."<<endl;
    return 0;
}

void testVectorFinal(int gridSpacing)
{
    //Perform the current PIVC implementation
    uint32 imageSizeX = 320; //Fixed because test image size is known
    uint32 imageSizeY = 240;

    int IPIPS = 2*gridSpacing;
    int xGrids, xGap, yGrids, yGap;

    xGrids = imageSizeX / gridSpacing;
    xGap = imageSizeX - xGrids*gridSpacing;
    yGrids = imageSizeY / gridSpacing;
    yGap = imageSizeY - yGrids*gridSpacing;

    struct VectorFinalParams vectorfinalparams;

    vectorfinalparams.meanAreaSize = 3; // CMAREAS
    vectorfinalparams.peakThreshold = 2; //peakThreshold
    vectorfinalparams.offsetx = ((xGap + IPIPS)/2.0) - 0.5;
    vectorfinalparams.offsety = ((yGap + IPIPS)/2.0) - 0.5;
    vectorfinalparams.gridSpacing = gridSpacing;
    vectorfinalparams.resetVal = 1; //resetval
    vectorfinalparams.xGrids = xGrids;
    vectorfinalparams.yGrids = yGrids;

    double *peaks, *velocityField, *velocityFieldAnswers;

    //Load the previously processed raw vectors
    peaks = new double[yGrids*xGrids*3*3];
    string imageDir = "./test/images-vecfinal/peaks.txt";
    loadTextData(imageDir, peaks);

    //Perform the current vector finalization
    velocityField = new double[yGrids*xGrids*10];
    fill_n(velocityField,yGrids*xGrids*10,0);
    VerifyFieldGrid(velocityField,peaks,vectorfinalparams);

    //Load the target results for vector finalization test
    velocityFieldAnswers = new double[yGrids*xGrids*10];
    imageDir = "./test/target-vecfinal/velocityfield.txt";
    loadTextData(imageDir, velocityFieldAnswers);

    //Compare the target results to the actual results
    ofstream myFile;
    myFile.open("./test/results/Vector Finalization Test Results.txt",ios::out);
    myFile<<"Vector Finalization Test Results"<<endl;
    bool results = true;
    for(int i = 0; i < yGrids*xGrids*10; i++) //compare the results
    {
        if(1e-6 < abs(velocityField[i] - velocityFieldAnswers[i]))
        {
            myFile<<"Test Failed at index = "<<i<<" with a diff of "<<abs(velocityField[i] - velocityFieldAnswers[i])<<endl;
            results = false;
        }
    }
    if(results)
    {
        myFile<<".....Passed"<<endl;
        myFile<<"All Vector Finalization Tests Passed"<<endl;
    }
    else
    {
        myFile<<".....Failed. See above for linear index of failure. Please note that the dimensions of the velocity field array was "<<yGrids*xGrids<<" by 10."<<endl;
    }
    myFile.close();
}

void testVectorComp(int gridSpacing)
{
    //Perform the current PIVC implementation
    uint32 imageSizeX = 320; //Fixed because test image size is known
    uint32 imageSizeY = 240;

    struct VectorCompParams vectorcompparams;
    vectorcompparams.interWindowSize = 2*gridSpacing; //IPIPS
    vectorcompparams.searchWindowSize = 2*vectorcompparams.interWindowSize; //SPIPS
    vectorcompparams.gridSpacing = gridSpacing; //gridspacing
    vectorcompparams.extendedSizeX = imageSizeX + vectorcompparams.searchWindowSize; //extendedSizeX
    vectorcompparams.xGrids = imageSizeX / gridSpacing; //xgrids
    vectorcompparams.yGrids = imageSizeY / gridSpacing; //ygrids

    int extendedSizeY = imageSizeY + vectorcompparams.searchWindowSize; //extendedSizeY

    int xGap = imageSizeX - vectorcompparams.xGrids*gridSpacing;
    int yGap = imageSizeY - vectorcompparams.yGrids*gridSpacing;

    vectorcompparams.offsetx = -0.5 + ((vectorcompparams.extendedSizeX - imageSizeX) + xGap + vectorcompparams.interWindowSize)/2.0; //xoffset
    vectorcompparams.offsety = -0.5 + ((extendedSizeY - imageSizeY) + yGap + vectorcompparams.interWindowSize)/2.0; //yoffset

    double *peaks, *imageA, *imageB, *peaksAnswers;

    //Load the previously preprocessed images
    imageA = new double[extendedSizeY*vectorcompparams.extendedSizeX];
    string imageDir = "./test/images-veccomp/imageOne.txt";
    loadTextData(imageDir, imageA);

    imageB = new double[extendedSizeY*vectorcompparams.extendedSizeX];
    imageDir = "./test/images-veccomp/imageTwo.txt";
    loadTextData(imageDir, imageB);

    //Perform the current vector computation
    peaks = new double[vectorcompparams.yGrids*vectorcompparams.xGrids*3*3];
    fill_n(peaks,vectorcompparams.yGrids*vectorcompparams.xGrids*3*3,0);
    DoWholeField(peaks,imageA,imageB,vectorcompparams);

    //Load the target results for vector computation test
    peaksAnswers = new double[vectorcompparams.yGrids*vectorcompparams.xGrids*3*3];
    imageDir = "./test/target-veccomp/peaks.txt";
    loadTextData(imageDir, peaksAnswers);

    //Compare the target results to the actual results
    ofstream myFile;
    myFile.open("./test/results/Vector Computation Test Results.txt",ios::out);
    myFile<<"Vector Computation Test Results"<<endl;
    bool results = true;
    for(int i = 0; i < vectorcompparams.yGrids*vectorcompparams.xGrids*3*3; i++) //compare the results
    {
        if(1e-6 < abs(peaks[i] - peaksAnswers[i]))
        {
            myFile<<"Test Failed at index = "<<i<<" with a diff of "<<abs(peaks[i] - peaksAnswers[i])<<endl;
            results = false;
        }
    }
    if(results)
    {
        myFile<<".....Passed"<<endl;
        myFile<<"All Vector Computation Tests Passed"<<endl;
    }
    else
    {
        myFile<<".....Failed. See above for linear index of failure. Please note that the dimensions of peaks was "<<vectorcompparams.yGrids<<" by "<<vectorcompparams.xGrids<<" by 3 by 3."<<endl;
        myFile<<"In addition, note that the 4D indexing is performed as (y,x,w,z) where x is the fastest changing variable."<<endl;
    }
    myFile.close();
}

void testPreProcess(int gridSpacing)
{
    uint32 imageSizeX;
    uint32 imageSizeY;

    //Perform the current PIVC implementation
    struct PreProcessParams preprocessparams;
    preprocessparams.imageSizeY = 240; //Fixed because test image size is known
    preprocessparams.imageSizeX = 320;
    preprocessparams.extendedSizeX = preprocessparams.imageSizeX + 4*gridSpacing;
    preprocessparams.extendedSizeY = preprocessparams.imageSizeY  + 4*gridSpacing;
    preprocessparams.minGrayLevel = 0;
    preprocessparams.maxGrayLevel = 200;

    double *imageA, *imageB, *imageOneRaw, *imageTwoRaw;

    string imageDir = "./test/images-preprocess/";
    vector<string> fileList;
    int numFiles = 0;
    getFiles(fileList, imageDir, numFiles);

    imageOneRaw = myTiffRead(fileList[0],imageSizeX,imageSizeY); //Load a pair of image files
    imageTwoRaw = myTiffRead(fileList[1],imageSizeX,imageSizeY);

    imageA = new double[preprocessparams.extendedSizeY*preprocessparams.extendedSizeX]; //initialize preprocessed images
    fill_n(imageA,preprocessparams.extendedSizeY*preprocessparams.extendedSizeX,0);
    imageB = new double[preprocessparams.extendedSizeY*preprocessparams.extendedSizeX];
    fill_n(imageB,preprocessparams.extendedSizeY*preprocessparams.extendedSizeX,0);

    preProcessImage(imageA, imageOneRaw, preprocessparams);
    preProcessImage(imageB, imageTwoRaw, preprocessparams);

    //Load the target results for preprocessing test
    double* answerA = new double[preprocessparams.extendedSizeY*preprocessparams.extendedSizeX];
    imageDir = "./test/target-preprocess/imageOne.txt";
    loadTextData(imageDir, answerA);

    double* answerB = new double[preprocessparams.extendedSizeY*preprocessparams.extendedSizeX];
    imageDir = "./test/target-preprocess/imageTwo.txt";
    loadTextData(imageDir, answerB);

    //Compare the target results to the actual results
    int i = 0;
    ofstream myFile;
    myFile.open("./test/results/Preprocessing Test Results.txt",ios::out);
    myFile<<"Preprocessing Test Results"<<endl;
    myFile<<"Image One";
    bool imageOneResult = true;
    for(int j = 0; j < preprocessparams.extendedSizeY; j++) //compare the first image in the pair
    {
        for(int k = 0; k < preprocessparams.extendedSizeX; k++)
        {
            if(1e-6 < abs(imageA[sub2Dind(j, k, preprocessparams.extendedSizeX)] - answerA[i]))
            {
               myFile<<"Test Failed at row = "<<j<<" and column = "<<k<<" with a diff of "<<abs(imageA[sub2Dind(j, k, preprocessparams.extendedSizeX)] - answerA[i])<<" i = "<<i<<endl;
               imageOneResult = false;
            }
            i++;
        }
    }
    if(imageOneResult)
        myFile<<".....Passed"<<endl;
    else
        myFile<<".....Failed. See above for pixels of failure."<<endl;

    i = 0;
    myFile<<"Image Two";
    bool imageTwoResult = true;
    for(int j = 0; j < preprocessparams.extendedSizeY; j++) //compare the second image in the pair
    {
        for(int k = 0; k < preprocessparams.extendedSizeX; k++)
        {
            if(1e-6 < abs(imageB[sub2Dind(j, k, preprocessparams.extendedSizeX)] - answerB[i]))
            {
               myFile<<"Test Failed at row = "<<j<<" and column = "<<k<<" with a diff of "<<abs(imageB[sub2Dind(j, k, preprocessparams.extendedSizeX)] - answerB[i])<<" i = "<<i<<endl;
               imageTwoResult = false;
            }
            i++;
        }
    }
    if(imageTwoResult)
    {
        myFile<<".....Passed"<<endl;
        myFile<<"All Preprocessing Tests Passed"<<endl;
    }
    else
        myFile<<".....Failed. See above for pixels of failure."<<endl;

    myFile.close();
}

void loadTextData(string filename, double* data)
{
    ifstream myFile;
    myFile.open(filename, ios::in);
    double element = 0.0;
    int i = 0;
    if(myFile.is_open())
    {
        while(myFile >> element)
        {
            data[i] = element;
            i++;
        }
    }
    myFile.close();
}
