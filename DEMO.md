# Introduction

PIVC is a widely applicable tool to be used as part of a more comprehensive PIV flow analysis. In the past, PIVC has been used to analyze numerous experimental flows with wide reaching applications such as [ocean wave dynamics](https://doi.org/10.1007/s10236-008-0132-y), [insect aerodynamics](https://doi.org/10.1016/j.expthermflusci.2022.110734), [marine hydrodynamics](https://doi.org/10.1088/0957-0233/18/1/012), [renewable energy](https://doi.org/10.1002/we.1895), and [atmospheric flows](https://doi.org/10.1016/j.jweia.2021.104605). The demonstration files included in PIV focus on two experimental flows, the canoncal turbulent boundary layer and the wake of a moth. A statistical analysis based on synthetic images of a Rankine vortex flow and Rankine vortex embedded in a Couette flow is also performed in this demonstration. 

## Usage

In order to run the PIVC demonstration follow these steps. 

1. [Download the PIVC release](https://gitlab.com/fibreglass/pivc/-/releases) or [compile PIVC](https://gitlab.com/fibreglass/pivc/-/blob/master/COMPILING.md) for your system. Make sure to include the repository (source code) when downloading PIVC.
2. After extracting, move the "demo" folder from the repository into the extracted "glinux" or "win32" folder from the release. This will place the "demo" folder in the same directory as PIVC, *.dll dependencies (Windows only), and the demo shell script (LinuxDemo.sh or WindowsDemo.bat).
3. Run the demo shell script.

# Boundary Layer Flow

The canonical boundary layer flow is one of the fundamental flow configurations in fluid dynamics. The behavior of this flow is critical to the current knowledge and advancement of numerous engineering systems (e.g. renewable energy) and natural phenomena (e.g. atmospheric flows). The image pairs included in this demonstration (see "/demo/BL images" folder in the repository) are from a typical turbulent boundary layer flow experiment. The first image pair is from the conventional perspective (x-y plane) of the turbulent boundary layer flow as sketched in <a href="#F1">Figure 1.</a> 

<h2 id="F1"></h2>
<img src="/docsrc/DemoFig1.png" alt="Figure 1" title="Figure 1" width="600"/> <figcaption>Figure 1: Sketch of turbulent boundary layer configuration.</figcaption>

The experimental images have cropped in order to focus on the boundary layer portion of the flow where the velocity profile, U(y), rapidly decreases toward zero at the wall. In this demonstration this image pair is processed with PIVC using 16 pixel grid spacing (32 px by 32 px interrogation window). When processed the results appear as a flow moving from left to right, similar to the vector field shown in <a href="#F2">Figure 2</a>. 

<h2 id="F2"></h2>
<img src="/docsrc/DemoFig2.png" alt="Figure 2" title="Figure 2" width="600"/> <figcaption>Figure 2: PIVC computed vector field of turbulent boundary layer in the x-y plane. Momentum thickness Reynolds number of 700.</figcaption>

The second image pair in the boundary layer flow demonstration is also from a turbulent boundary layer however it is from the perspective where the viewer is looking down at the flow at a plane 3.5 mm above the wall, hence the field of view is parallel to the x-z plane. Here, the mean flow is in the x-direction, vertical in <a href="#F3">Figure 3</a>. This perspective enables an analysis of span-wise flow phenomena. This image pair when analyzed at 16 grid spacing using PIVC produces a vector field where the flow is moving primarily from bottom to top of the field of view similar to <a href="#F3">Figure 3</a>. 

<h2 id="F3"></h2>
<img src="/docsrc/DemoFig3.png" alt="Figure 3" title="Figure 3" width="600"/> <figcaption>Figure 3: PIVC computed vector field of turbulent boundary layer in the x-z plane. Momentum thickness Reynolds number of 1000.</figcaption>

The deviations from the mean flow direction in Figure 2 and Figure 3 are due to the contributions of local turbulent phenomena. These affects become clearly visible after Reynolds decomposition and other advanced analyses are performed which have been the subject of numerous [previous investigations](https://doi.org/10.1016/j.euromechflu.2022.05.012) where PIVC was utilized.

# Moth Wake Flow

The dynamics of insect flight is a field of study of great interest to the advancement of aerodynamics in areas such as the development of unmanned micro-aerial vehicles. In order to advance the current knowledge of aerodynamics and flight, comprehensive studies on the flow mechanisms associated with the flight of insects and birds are required. A [recent study](https://doi.org/10.1016/j.expthermflusci.2022.110734) investigated the dynamics of the Pseudaletia Unipuncta moth species using PIVC to perform image analysis. The experimental image pair from this study included in this demonstration is stored in the "/demo/moth images" folder. This image pair depicts the moth in flight as viewed from behind. When these images are analyzed using PIVC with 32 pixel grid spacing, a vector field similar to <a href="#F4">Figure 4</a> is produced. The vector field shown in the Figure has had post-processing performed.

<h2 id="F4"></h2>
<img src="/docsrc/DemoFig4.png" alt="Figure 4" title="Figure 4" width="600"/> <figcaption>Figure 4: PIVC computed vector field of turbulent wake produced by a moth. Post-processing has been applied to this vector field and spurious vectors produced by the moth's body were removed.</figcaption>

In the top center of this vector field is the body of the moth. Immediately below the moth is a turbulent wake produced by the flapping motion of the moth's wings. The dynamics of this wake region was thoroughly characterized through advanced flow analysis techniques. 

# Synthetic Images

To quantify the accuracy of PIVC, two types of synthetic image are provided in this demonstration. One type is the Rankine vortex images that are stored in the "/demo/vortex images" folder. The other type is a uni-directional shear (Couette) flow with an embedded vortex whose images are in the "/demo/shear images" folder. All images were generated from the known analytical flow field in the vortex and Couette flow with vortex. The MATLAB script used to generate these images is based on the work by [lu-p](https://github.com/lu-p/standard-PIV-image-generator) and is included in the demo folder. The image pairs generated were analyzed using PIVC with a grid spacing of 12 pixels. A sample vortex vector field is presented in <a href="#F5">Figure 5</a> and a sample vector field from the Couette flow with vortex in <a href="#F6">Figure 6</a>.

<h2 id="F5"></h2>
<img src="/docsrc/DemoFig5.png" alt="Figure 5" title="Figure 5" width="600"/> <figcaption>Figure 5: PIVC computed Rankine vortex vector field.</figcaption>

<h2 id="F6"></h2>
<img src="/docsrc/DemoFig6.png" alt="Figure 6" title="Figure 6" width="600"/> <figcaption>Figure 6: PIVC computed Couette flow with Rankine vortex vector field.</figcaption>

A MATLAB script is provided that performs a statistical comparison between the analytical solution and the PIVC output. This analysis is performed on each case using 5 vector fields. For users, the provided demo script must be run in order to generate the PIVC output. Then, the MATLAB script "compareResults.m" must be run after providing the required directories to the analytical solutions, PIVC grid spacing, and synthetic image size. This MATLAB script compares the analytical solution to the PIVC output for the two synthetic flow configurations and provides a short summary of results in the MATLAB console. For the image sets included in the demo, the median displacement difference between PIVC and the exact solution was less than 5 percent. The MATLAB scripts used to produce synthetic images ("generatesyntheticPIVimages.m", "generateVortexImage.m", and "generateVortexShearImage.m"), can be used by the user to generate additional synthetic images. By modifying line 8 (vortex) and line 22/23 (shear flow with vortex) in "generatesyntheticPIVimages.m" it is possible to change the characteristics of the resulting exact displacement field such as the vortex rotation direction, vortex strength, shear flow direction and shear flow max displacement. The "compareResults.m" script is compatible with the script to generate synthetic images and only needs to be specified the same parameters as the image generation script in order to perform a result comparison.