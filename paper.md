---
title: 'PIVC: A C/C++ Program for Particle Image Velocimetry Vector Computation'
tags:
  - fluid mechanics
  - particle image velocimetry 
  - image analysis
  - C/C++
authors:
  - name: Kadeem Dennis
    affiliation: 1 
  - name: Michael Marxen
    orcid: 0000-0001-8870-0041
    affiliation: 2
  - name: Kamran Siddiqui
    affiliation: 1
affiliations:
 - name: Department of Mechanical and Materials Engineering, University of Western Ontario, 1151 Richmond Street, London, Ontario, Canada, N6A 3K7
   index: 1
 - name: Department of Psychiatry and Neuroimaging Center, Technische Universität Dresden, Würzburger Straße 35, 01187 Dresden, Germany 
   index: 2
date: 06 August 2021
bibliography: paper.bib

---

# Summary

Fluid dynamics is an extremely broad area of study where the motion of fluids is investigated and characterized across a wide range of length and time scales. In a majority of practical applications, fluid flow is turbulent in nature, a condition where the fluid motion is highly three-dimensional and stochastic. While the Navier-Stokes equations (governing equations in fluid mechanics) are applicable to all fluid flows, determining a general solution for these equations for turbulent flows is computationally challenging and mathematically impossible. Hence, experimental research is a leading contributor to the advancement of knowledge on fluid dynamics. Experimental research is heavily reliant on the tools and techniques of performing measurements of fluid phenomena. Fluid velocity is one of the fundamental variables and a parameter of interest in fluid mechanics. Hence, knowing the fluid velocity field in time and/or space is often the primary objective in fluid dynamics research. Thus, most of the tools and techniques used in experimental fluid mechanics research are used to measure the fluid velocity in a given flow. 

The planar Particle Image Velocimetry (PIV) technique is one well established optical technique that remotely measures fluid velocity (two velocity components in a 2D plane). The operation of the planar PIV technique is based upon capturing images of reflective seed particles premixed in the flow, as they travel through a region of interest illuminated by a thin sheet of high-intensity light (typically produced by a laser). Pairs of images containing illuminated seed particles are recorded with a preset time delay between the two images in the pair, such that the positions of the particles are shifted between the two images due to the fluid motion. By cross-correlating the two images (a mathematical operation), the average particle displacement can be estimated. With the estimated particle displacement and known time delay between the images in the pair, the fluid velocity (two velocity components) is estimated. This estimate can be improved by dividing each image into several sub-sections, i.e interrogation windows (in the first image) and search windows (in the second image), and performing the cross-correlation operation on each section. This provides a spatial distribution of fluid velocities in the region of interest (measurement plane). By acquiring and processing a sequence of image pairs, the technique provides fluid velocity fields in both space and time.

PIVC is an open source implementation and modernization of the PIV analysis algorithm described by Marxen [@MarxenThesis; @MarxenEIF]. PIVC performs the mathematical operations and analysis to generate a two-dimensional velocity field from pairs of illuminated seed particle images. Specifically PIVC performs the Fast Fourier Transform based cross-correlation to determine the local velocity. The fluid velocity is computed at sub-pixel accuracy using a three-point Gaussian estimator [@MarxenEIF]. In order to improve the spatial resolution of computed velocity vectors, the square interrogation and search windows are overlapped by 50% and a local median filter is used to identify and correct erroneous vectors. 

# Statement of need

Presently, experimental researchers seeking to utilize PIV in their research labs have numerous options. There are several commercial systems that are fully inclusive where both hardware and software needed to perform PIV experiments are provided. The software to perform the PIV mathematical analysis for most of these commercial systems is closed source and proprietary. Periodically, the performance of a selection of these commercial programs is investigated and reported [@Kahler]. In addition, there are a number of free and open source PIV analysis programs, documented in Table 1.

Table 1: Survey of existing open source PIV analysis software.

|Name | Language | Most Recent Update |
|:---------:|:---------:|:---------:|
| AnaPIV [@AnaPIV] | Pascal | 2016 |
| GPIV [@GPIV] | C | 2009 |
| JPIV [@JPIV] | Java | 2020 |
| MPIV [@MoriChang2003] | MATLAB | 2012 |
| PIVLAB [@Thielicke_2021] | MATLAB | 2021 |
| PPIV [@PPIV] | C | 2009 |
| OpenPIV [@OpenPIV] | Python/MATLAB | 2021 |
| OSIV  [@OSIV] | C | 2009 |

Of the open source PIV analysis programs, several have not been updated in over 10 years. To our knowledge the currently maintained software packages are primarily written in MATLAB, a resource intensive, commercial, closed source software suite. The only remaining currently maintained and free options are, (i) OpenPIV [@OpenPIV], which is presently transitioning away from MATLAB and towards Python, (ii) JPIV [@JPIV] written in Java, and (iii) AnaPIV [@AnaPIV] written in Pascal. Currently, there is no maintained, free and open source PIV analysis program written in C/C++. PIVC addresses this need in the open source PIV research community by providing a free, modern, and efficient implementation of PIV mathematical analysis that utilizes OpenMP to scale workloads across multiple CPU cores. The mathematical analysis algorithm implemented in PIVC has been extensively used in previous and ongoing research studies [@Siddiqui2001;@Gajusingh;@ElatarIJHFF;@Jevnikar2019;@Nasim2021;@Dennis2021IJHFF;@Dennis2022EJMF;@MarxenThesis;@Siddiqui2007;@Siddiqui2007-2;@Bukhari2008;@Greig2015;@Kilpatrick;@Hashemi-Tari;@Shaikh;@Sookdeo;@Jaroslawski] and it is anticipated that PIVC will continued to be used and developed by the research community. 

# Acknowledgments

The authors would like to thank the University of Western Ontario, the Natural Sciences & Engineering Research Council of Canada (NSERC), and the Ontario Ministry of Training, Colleges & Universities for their support.

# References