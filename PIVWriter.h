#ifndef PIVWRITER_H
#define PIVWRITER_H

#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <zlib.h>
#include <algorithm>

extern "C" {
#include "inputParser.h"
}

using namespace std;

//classes
class PIVWriter
{
    public:
        PIVWriter(DataOrder order = imageproc, string outDir = "");
        void setStreamLength(int numFiles);
        virtual int writeData(int fileCounter, double* data, int sizeX, int sizeY, int sizeZ) = 0;

    protected:
        DataOrder myOrder;
        int streamLength, fileCounter = 1;
        string outPrefix;
};

class PlainPIVWriter: public PIVWriter
{
    public:
        PlainPIVWriter(DataOrder order = imageproc, string outDir = ""):PIVWriter(order,outDir) { }

        int writeData(int fileCounter, double* data, int sizeX, int sizeY, int sizeZ);
};

class GZPIVWriter: public PIVWriter
{
    public:
        GZPIVWriter(DataOrder order = imageproc, string outDir = ""):PIVWriter(order,outDir) { }

        int writeData(int fileCounter, double* data, int sizeX, int sizeY, int sizeZ);
};

#endif // DOWHOLEFIELD_H
