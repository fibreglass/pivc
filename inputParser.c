#include "inputParser.h"

const char *argp_program_version =
  "PIVC: A C/C++ Program for Particle Image Velocimetry Vector Computation. Version 1.2";
const char *argp_program_bug_address =
  "https://gitlab.com/fibreglass/pivc";

static char doc[] =
  "PIVC computes vector fields from given image pairs. \n Example usage: ./PIVC -g 16 -c plain -s imageproc -i \\my\\image\\path -o \\my\\output\\path\\filename \n Command line parameters are as follows."; //basic documentation

static char args_doc[] = ""; //Description of accepted arguments

//Order of fields: {NAME, KEY, ARG, FLAGS, DOC}.
static struct argp_option options[] = {
  {"gridSpacing",      'g', "GRID",  0,  "Grid Spacing: Distance between adjacent vectors measured in pixels. Default = 16. Valid options = 8, 12, 16, 24, 32." },
  {"compression",      'c', "COMP",  0,  "Compressed or uncompressed output files. Options = 'plain' or 'gz', case-sensitive. Default = 'plain'." },
  {"coordinateSystem", 's', "COORDS",  0, "Output vectors using image processing or Cartesian coordinates. Options = 'imageproc' or 'cartesian', case-sensitive. Default = 'imageproc'." },
  {"inputDirectory",   'i', "INDIR", 0, "Full path to a folder containing your input images. Default = current directory." },
  {"outputFilename",   'o', "FILENAME", 0,      "Full path to a folder where you want your data to be written including the desired filename. Default = current directory + pivdata." },
  { 0 }
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  /* Get the input argument from argp_parse, which we know is a pointer to our arguments structure. */
  struct arguments *arguments = state->input;
  int gridinput;
  switch (key)
    {
    case 'g':
      gridinput = atoi(arg);
      if(gridinput != 16 && gridinput != 24 && gridinput != 32 && gridinput != 12 && gridinput != 8)
          argp_error(state, "Invalid grid spacing.");
      else
        arguments->grid = atoi(arg);
      break;
    case 'c':
      if(strcmp("gz", arg) == 0)
        arguments->argComp = gz;
      else
        arguments->argComp = plain;
      break;
    case 's':
      if(strcmp("cartesian", arg) == 0)
        arguments->argOrder = cartesian;
      else
        arguments->argOrder = imageproc;
      break;
    case 'i':
      arguments->input_path = arg;
    case 'o':
      arguments->output_path = arg;
      break;

    case ARGP_KEY_ARG:
      if (state->arg_num >= 0)
        /* Too many arguments. */
        argp_usage (state);

      arguments->args[state->arg_num] = arg;
      break;

    case ARGP_KEY_END:
      if (state->arg_num < 0)
        /* Not enough arguments. */
        argp_usage (state);
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};

struct arguments inputHandler(int argc, char **argv)
{
    struct arguments arguments;
    arguments.grid = 16;
    arguments.argOrder = imageproc;
    arguments.argComp = plain;
    arguments.output_path = "./pivdata";
    arguments.input_path = "./";

    argp_parse (&argp, argc, argv, 0, 0, &arguments);

    return arguments;
}
