#include <stdlib.h>
#include <argp.h>
#include <string.h>
#include <unistd.h>

enum DataOrder {cartesian=0, imageproc};
enum FileCompression {plain=0, gz};

struct arguments
{
  char *args[1];  // no arguments needed
  int grid;    //option for grid spacing
  enum DataOrder argOrder; //option for coordinate system
  enum FileCompression argComp; //option for file compression
  char *output_path, *input_path; // options for inputdirectory and outputfilename
};

struct arguments inputHandler(int argc, char **argv);
